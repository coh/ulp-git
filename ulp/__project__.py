"""provide a centralized point to retrieve the version info
"""

_vinfo = {
    'major': 0,
    'minor': 8,
    'patch': 0,
    'alpha': 0,
    'beta': 1,
    'rc': 0,
}

# do not change anything below this line unless you absolutely know what
# you're doing


LICENSE = '''
Copyright (c) 2010, Clemens-O. Hoppe and Contributors.

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of ulp nor the names of its contributors may be used to
  endorse or promote products derived from this software without specific
  prior written permission.


THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''


def version():
    """ construct and return a PEP 386-compatible version string.
    """
    def _is(prerel):
        return prerel in _vinfo.keys() and _vinfo[prerel]

    vstr = '.'.join(str(x) for x in version_tuple())
    prereldict = {
        'alpha': 'a',
        'beta': 'b',
        'rc': 'c'
    }

    for key, value in prereldict.items():
        if _is(key):
            vstr += value + str(_vinfo[key])
            break

    return vstr


def version_tuple():
    """ return a version tuple only containing (major, minor, patch)
    """
    return version_tuple_extended()[:3]


def version_tuple_extended():
    """ same as `version_tuple`_ but also return ('alpha', 'beta', 'rc')
    """
    return tuple((_vinfo.get(x, 0) for x in ('major', 'minor', 'patch',
                                             'alpha', 'beta', 'rc')))


if __name__ == '__main__':
    print version()
    print LICENSE
