import sys


class StdinSource(object):
    def __init__(self):
        self._source = sys.stdin

    def next(self):
        return self._source.next()

    def __iter__(self):
        return self._source.__iter__()

    def real_source(self):
        return self._source
