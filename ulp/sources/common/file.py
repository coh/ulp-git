class FileSource(object):
    def __init__(self, filename, mode='r'):
        self._source = open(filename, mode)

    def next(self):
        return self._source.next()

    def __iter__(self):
        return self._source.__iter__()
