import socket


def _receive_messages(addr, maxsize=100000, remote_addr=None):
    soc = None
    data = ''
    try:
        soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except Exception as e:
        raise e
    try:
        soc.bind(addr)
    except Exception as e:
        raise e

    while True:
        try:
            (msg, addr) = soc.recvfrom(maxsize)
        except Exception as e:
            raise e

        #got a spoofed message?
        if ((remote_addr and addr != remote_addr) or
                not msg.startswith('\xFF' * 4 + 'n')):
            continue
        else:
            data += msg.replace('\xFF' * 4 + 'n', '').replace('\x00', '')
        pos = data.rfind('\n')
        if pos != -1:
            lines = data[:pos + 1].split('\n')
            data = data[pos + 1:]

            for line in lines:
                yield line
    soc.close()


class UdpSource(object):
    def __init__(self, local_ip='', port=26000, remote_ip=''):
        self._ip = local_ip
        self._port = port
        self._remote_ip = remote_ip

    def __iter__(self):
        """ ugly hack to turn the generator function into an iterable object
        """
        return _receive_messages((self._ip, self._port),
                                 remote_addr=self._remote_ip)
