from __future__ import with_statement

import operator

from ulp.helpers.common.imports import import_classes


class ChunkedParser(object):
    """generic chunked parser loader and executor
    """
    def _add_or_repl_observer(self, new_obs):
        #we'll have to use that form of iterating,
        #since we might be changing some entry
        for i in range(0, len(self._observers)):
            if self._observers[i].hook_name() == new_obs.hook_name():
                self._observers[i] = new_obs
                return

        self._observers.append(new_obs)

    def __init__(self, pathname, repl_or_ext_pathname=None):
        super(ChunkedParser, self).__init__()
        self._hooks = dict()
        self._observers = [x() for x in
                           import_classes(pathname, attribute='match_line')]
        if repl_or_ext_pathname:
            repl_or_ext = [x() for x in
                           import_classes(repl_or_ext_pathname,
                                          attribute='match_line')]
            #FIXME: replace this by some nice set() math
            for x in repl_or_ext:
                self._add_or_repl_observer(x)

        can_do_priority_sort = True
        for x in self._observers:
            if not hasattr(x, 'parse_priority'):
                can_do_priority_sort = False
                break
        if can_do_priority_sort:
            tmp = sorted(self._observers,
                         key=operator.methodcaller('parse_priority'),
                         reverse=True)
            self._observers = tmp

    def invoke_hooks(self, event, data):
        if event not in self._hooks.keys():
            return
        for hook in self._hooks[event]:
            hook(self, data)

    def set_hook(self, event, callable_or_list):
        if (not isinstance(callable_or_list, list) and
            not isinstance(callable_or_list, tuple) and
                hasattr(callable_or_list, '__call__')):
            self._hooks[event] = [callable_or_list]
        else:
            self._hooks[event] = callable_or_list

    def parse(self, line):
        self.invoke_hooks('<new_line>', line)
        for observer in self._observers:
            if observer.match_line(self, line):
                break
