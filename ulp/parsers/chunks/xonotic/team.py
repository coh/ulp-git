import re
from ulp.helpers.nexuiz import convert_default_entries

_team_regex = re.compile(r'^(?:\s|\^7)?:team:(?P<playerid>.+):(?P<team>.+)'
                         r':(?P<jointype>.+)$')


class TeamChunk(object):
    @staticmethod
    def _get_team_results(match):
        if match:
            return convert_default_entries(match.groupdict())

    @staticmethod
    def _is_team(line):
        return _team_regex.match(line)

    @staticmethod
    def hook_name():
        return 'team'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_team(line)
        if tmp:
            res = self._get_team_results(tmp)
            caller.invoke_hooks('team', res)
        return tmp
