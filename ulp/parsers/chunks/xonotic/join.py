import re
import socket

from ulp.helpers.nexuiz import convert_default_entries

#TODO add IPv6 eventually
_join_regex = re.compile(r'^(?:\s|\^7)?:join:(?P<playerid>\d+):(?P<slot>\d+)'
                         r':(?P<ip>bot|local|(?:\d{1,3}\.){3}\d{1,3}):'
                         r'(?P<name>.*)$')


class JoinChunk(object):
    @staticmethod
    def _is_join(line):
        tmp = _join_regex.match(line)
        if tmp and tmp.group('ip') not in ('local', 'bot'):
            try:
                socket.inet_aton(tmp.group('ip'))
            except socket.error:
                return False
        return tmp

    @staticmethod
    def _get_join_results(match):
        if match:
            res = match.groupdict()
            if res['ip'] == 'local':
                res['ip'] = '127.0.0.1'
            if res['ip'] == 'bot':
                res['human'] = False
                del res['ip']
            else:
                res['human'] = True
            return convert_default_entries(res)

    @staticmethod
    def hook_name():
        return ['join_ai', 'join_human']

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_join(line)
        if tmp:
            res = self._get_join_results(tmp)
            if res['human']:
                caller.invoke_hooks('join_human', res)
            else:
                caller.invoke_hooks('join_ai', res)
        return tmp
