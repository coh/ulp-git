import re
from ulp.helpers.nexuiz import convert_default_entries

_anticheat_regex = re.compile(r'^(?:\s|\^7)?:anticheat:(?P<type>.+)'
                              r':(?P<playerid>\d+):(?P<score>.+)$')


class AnticheatChunk(object):
    @staticmethod
    def _get_anticheat_results(match):
        if match:
            res = match.groupdict()
            try:
                res['score'] = float(res['score'])
            except ValueError:
                pass
            return convert_default_entries(res)

    @staticmethod
    def _is_anticheat(line):
        return _anticheat_regex.match(line)

    @staticmethod
    def hook_name():
        return 'anticheat'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_anticheat(line)
        if tmp:
            res = self._get_anticheat_results(tmp)
            caller.invoke_hooks('anticheat', res)
        return tmp
