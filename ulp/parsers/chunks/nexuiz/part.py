import re
from ulp.helpers.nexuiz import convert_default_entries

_part_regex = re.compile(r'^(?:\s|\^7)?:part:(?P<playerid>\d+)')


class PartChunk(object):
    @staticmethod
    def _is_part(line):
        return _part_regex.match(line)

    @staticmethod
    def _get_part_results(match):
        if match:
            return convert_default_entries(match.groupdict())

    @staticmethod
    def hook_name():
        return 'part'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_part(line)
        if tmp:
            res = self._get_part_results(tmp)
            caller.invoke_hooks('part', res)
        return tmp
