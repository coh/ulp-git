import re
from ulp.helpers.nexuiz import convert_default_entries

_ctf_player_event_regex = re.compile(r'^(?:\s|\^7)?:ctf:(?P<event>'
                                     r'steal|dropped|pickup|capture|return):'
                                     r'(?P<flagcolor>\d+):(?P<playerid>\d+)$')
_ctf_event_regex = re.compile(r'^(?:\s|\^7)?:ctf:(?P<event>returned):'
                              r'(?P<flagcolor>\d+)$')


class CtfChunk(object):
    @staticmethod
    def _get_ctf_player_event_results(match):
        if match:
            res = match.groupdict()
            if not 'playerid' in res.keys():
                res['playerid'] = None
            return convert_default_entries(res)

    @staticmethod
    def _is_ctf_player_event(line):
        return _ctf_player_event_regex.match(line)

    @staticmethod
    def _is_ctf_event(line):
        return _ctf_event_regex.match(line)

    @staticmethod
    def hook_name():
        return 'ctf_event'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_ctf_player_event(line)
        if not tmp:
            tmp = self._is_ctf_event(line)

        if tmp:
            res = self._get_ctf_player_event_results(tmp)
            caller.invoke_hooks('ctf_event', res)
        return tmp
