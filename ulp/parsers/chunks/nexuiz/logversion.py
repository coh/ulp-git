import re

_logversion_regex = re.compile(r'^(?:\s|\^7)?:logversion:(\d+)$')


class LogversionChunk(object):
    @staticmethod
    def _is_logversion(line):
        return _logversion_regex.match(line)

    @staticmethod
    def _get_logversion_results(match):
        if match:
            return int(match.group(1))

    @staticmethod
    def hook_name():
        return 'logversion'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_logversion(line)
        if tmp:
            res = self._get_logversion_results(tmp)
            caller.invoke_hooks('logversion', res)
        return tmp
