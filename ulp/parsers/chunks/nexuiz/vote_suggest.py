import re
from ulp.helpers.nexuiz import convert_default_entries

_vote_suggested_regex = re.compile(r'^(?:\s|\^7)?:vote:suggested:(?P<map>.+)'
                                   r':(?P<playerid>\d+)$')
_vote_suggestion_accepted_regex = re.compile(r'^(?:\s|\^7)?:vote:suggestion_'
                                             r'accepted:(?P<map>.*)$')


class VoteSuggestChunk(object):
    @staticmethod
    def _get_vote_suggested_results(match):
        if match:
            return convert_default_entries(match.groupdict())

    @staticmethod
    def _get_vote_suggestion_accepted_results(match):
        if match:
            return match.groupdict()

    @staticmethod
    def _is_vote_suggested(line):
        return _vote_suggested_regex.match(line)

    @staticmethod
    def _is_vote_suggestion_accepted(line):
        return _vote_suggestion_accepted_regex.match(line)

    @staticmethod
    def hook_name():
        return ['vote_suggested', 'vote_suggestion_accepted']

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_vote_suggested(line)
        if tmp:
            res = self._get_vote_suggested_results(tmp)
            caller.invoke_hooks('vote_suggested', res)
        else:
            tmp = self._is_vote_suggestion_accepted(line)
            if tmp:
                res = self._get_vote_suggestion_accepted_results(tmp)
                caller.invoke_hooks('vote_suggestion_accepted', res)
        return tmp
