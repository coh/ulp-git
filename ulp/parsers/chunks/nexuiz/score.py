import re

from ulp.helpers.nexuiz import convert_default_entries

_scores_regex = re.compile(r'^(?:\s|\^7)?:scores:(?P<type>.+?)_(?P<map>.+)'
                           r':(?P<time>\d+)$')
_label_regex = re.compile(r'^(?:\s|\^7)?:labels:(?P<type>.+?):(?P<fields>.+)$')
_playerscore_regex = re.compile(r'^(?:\s|\^7)?:player:see-labels:'
                                r'(?P<fields>.+?):(?P<time>\d+):(?P<team>.+):'
                                r'(?P<playerid>\d+):(?P<name>.+)$')
_teamscore_regex = re.compile(r'^(?:\s|\^7)?:teamscores:see-labels:'
                              r'(?P<fields>.*?):(?P<teamid>\d+)$')


class ScoreChunk(object):
    @staticmethod
    def _is_scores(line):
        return _scores_regex.match(line)

    @staticmethod
    def _get_score_results(match):
        if match:
            return convert_default_entries(match.groupdict(), {'time': int})

    @staticmethod
    def _is_label(line):
        return _label_regex.match(line)

    @staticmethod
    def _get_label_results(match):
        if match:
            res = match.groupdict()
            res['fields'] = [x for x in res['fields'].split(',') if x]
            return res

    @staticmethod
    def _is_playerscore(line):
        return _playerscore_regex.match(line)

    @staticmethod
    def _get_playerscore_results(match):
        if match:
            res = match.groupdict()
            res['fields'] = [int(x) for x in res['fields'].split(',')]
            return convert_default_entries(res, {'time': int})

    @staticmethod
    def _is_teamscore(line):
        return _teamscore_regex.match(line)

    @staticmethod
    def _get_teamscore_results(match):
        if match:
            res = match.groupdict()
            res['fields'] = [int(x) for x in res['fields'].split(',') if x]
            if not len(res['fields']):
                res['fields'] = None
            return convert_default_entries(res)

    @staticmethod
    def hook_name():
        return ['label', 'playerscore', 'scores', 'teamscore']

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_scores(line)
        if tmp:
            res = self._get_score_results(tmp)
            caller.invoke_hooks('scores', res)
            return tmp
        tmp = self._is_label(line)
        if tmp:
            res = self._get_label_results(tmp)
            caller.invoke_hooks('label', res)
            return tmp
        tmp = self._is_playerscore(line)
        if tmp:
            res = self._get_playerscore_results(tmp)
            caller.invoke_hooks('playerscore', res)
            return tmp
        tmp = self._is_teamscore(line)
        if tmp:
            res = self._get_teamscore_results(tmp)
            caller.invoke_hooks('teamscore', res)
            return tmp
