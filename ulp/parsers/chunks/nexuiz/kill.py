import re
from ulp.helpers.common.utils import make_dict

_kill_frag_or_tk_regex = re.compile(r'^(?:\s+|\^7)?:kill:(?P<type>frag|tk):'
                                    r'(?P<attackerid>\d+):(?P<victimid>\d+):'
                                    r'type=(?P<cod>\d+):'
                                    r'items=(?P<attackeritems>.*?):'
                                    r'victimitems=(?P<victimitems>.+?)$')
_kill_accident_or_suicide_regex = re.compile(r'^(?:\s+|\^7)?:kill:'
                                             r'(?P<type>suicide|accident):'
                                             r'(?P<attackerid>\d+):'
                                             r'(?P<victimid>\d+)'
                                             r':type=(?P<cod>\d+):'
                                             r'items=(?P<victimitems>.+?)$')
_playeritems = re.compile(r'^(?P<weapons>\d+)(?P<flags>[FSIT]{0,4})'
                          r'(\|(?P<runes>\d+))?$')


class KillChunk(object):
    @staticmethod
    def _is_kill_frag_or_tk(line):
        return _kill_frag_or_tk_regex.match(line)

    @staticmethod
    def _is_kill_accident_or_suicide(line):
        return _kill_accident_or_suicide_regex.match(line)

    @staticmethod
    def _playeritems_dict(itemstring):
        match = _playeritems.match(itemstring)
        if match:
            keys = ['weapons', 'flags', 'runes']
            values = []
            for x in keys:
                try:
                    values.append(match.group(x))
                except Exception:
                    values.append(None)
            for i in range(2):
                if values[i * 2]:
                    values[i * 2] = int(values[i * 2])
            if len(values[1]) == 0:
                values[1] = None
            return make_dict(keys, values)
        else:
            return None

    @staticmethod
    def _get_kill_frag_or_tk_results(match):
        if match:
            res = match.groupdict()
            for key in ('attackerid', 'victimid', 'cod'):
                res[key] = int(res[key])
            for key in ('attackeritems', 'victimitems'):
                res[key] = KillChunk._playeritems_dict(res[key])
            return res

    @staticmethod
    def _get_kill_accident_or_suicide_results(match):
        if match:
            res = match.groupdict()
            for key in ('attackerid', 'victimid', 'cod'):
                res[key] = int(res[key])
            res['victimitems'] = KillChunk._playeritems_dict(
                res['victimitems'])
            return res

    @staticmethod
    def hook_name():
        return ['kill_as', 'kill_ft']

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_kill_frag_or_tk(line)
        if tmp:
            res = self._get_kill_frag_or_tk_results(tmp)
            caller.invoke_hooks('kill_ft', res)
        else:
            tmp = self._is_kill_accident_or_suicide(line)
            if tmp:
                res = self._get_kill_accident_or_suicide_results(tmp)
                caller.invoke_hooks('kill_as', res)
        return tmp
