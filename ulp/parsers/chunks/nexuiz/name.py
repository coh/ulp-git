import re

from ulp.helpers.nexuiz import convert_default_entries, name_to_utf8

_name_regex = re.compile(r'^(?:\s|\^7)?:name:(?P<playerid>\d+)'
                         r':(?P<newname>.+)$')


class NameChunk(object):
    @staticmethod
    def _is_name(line):
        return _name_regex.match(line)

    @staticmethod
    def _get_name_results(match):
        if match:
            res = match.groupdict()
            res['newname'] = name_to_utf8(res['newname'])
            return convert_default_entries(res)

    @staticmethod
    def hook_name():
        return 'name'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_name(line)
        if tmp:
            res = self._get_name_results(tmp)
            caller.invoke_hooks('name', res)
        return tmp
