import datetime
import re

_time_regex = re.compile(r'^(?:\s|\^7)?:time:(.+)$')
_time_string = "%Y-%m-%d %H:%M:%S"


class TimeChunk(object):
    @staticmethod
    def _get_time(match):
        return match

    @staticmethod
    def _is_time(line):
        res = _time_regex.match(line)
        if res:
            tim = datetime.datetime.now()
            try:
                tim = tim.strptime(res.group(1), "%Y-%m-%d %H:%M:%S")
            except ValueError:
                return None
            return tim
        return None

    @staticmethod
    def hook_name():
        return 'time'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_time(line)
        if tmp:
            caller.invoke_hooks('time', tmp)
        return tmp
