import re

_gameover_regex = re.compile(r'^(?:\s|\^7)?:gameover$')


class GameoverChunk(object):
    @staticmethod
    def _is_gameover(line):
        return _gameover_regex.match(line)

    @staticmethod
    def hook_name():
        return 'gameover'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_gameover(line)
        if tmp:
            caller.invoke_hooks('gameover', [])
        return tmp
