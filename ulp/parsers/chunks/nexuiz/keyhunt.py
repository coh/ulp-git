import re

_keyhunt_regex = re.compile(r'^(?:\s|\^7)?:keyhunt:'
                            r'(?P<eventtype>capture|carrierfrag|collect|'
                            r'destroyed|destroyed_holdingkey|dropkey|losekey|'
                            r'push|pushed):(?P<playerid>\d+):'
                            r'(?P<playerpoints>\d+):(?P<keyownerid>\d+):'
                            r'(?P<keyownerpoints>\d+):(?P<teamkey>.+)$')


class KeyhuntChunk(object):
    @staticmethod
    def _get_keyhunt_results(match):
        if match:
            res = match.groupdict()
            for key in res.keys():
                if key not in ('eventtype', 'teamkey'):
                    res[key] = int(res[key])
            return res

    @staticmethod
    def _is_keyhunt(line):
        return _keyhunt_regex.match(line)

    @staticmethod
    def hook_name():
        return 'keyhunt'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_keyhunt(line)
        if tmp:
            res = self._get_keyhunt_results(tmp)
            caller.invoke_hooks('keyhunt', res)
        return tmp
