import operator
import re
from ulp.helpers.common.utils import make_dict

_mapvote_finished_regex = re.compile(r'^(?:\s+|\^7)?:vote:finished:(.+)$')
_mapvote_keeptwo_regex = re.compile(r'^(?:\s+|\^7)?:vote:keeptwo:(.*)$')


def _map_list_to_dict(lis):
    """ map [m,v,m,v,m,v] to {m:int(v), m:int(v), m:int(v)}
    """
    return make_dict(lis[::2], [int(x) for x in lis[1::2]])


def _helper_mapvote_split_match_chunk(match):
    """ split foo:::b:a:r into [[foo], [b,a,r]]
    """
    tmp = match.split(':::')
    if tmp == ['']:
        return []
    res = []
    for chunk in tmp:
        res.append((chunk.split(':')))
    return res


def _helper_mapvote_convert_sort_append_chunks(list_, chunk):
    """ append a value-sorted version of [m,v,m,v] to list_ and return it
    """
    tmpdict = _map_list_to_dict(chunk)
    valuesorted_dict = sorted(tmpdict.items(),
                              key=operator.itemgetter(1), reverse=True)
    for key, value in valuesorted_dict:
        list_.append((key, value))
    return list_


def _helper_mapvote_append_chunks(list_, chunks):
    list_ = _helper_mapvote_convert_sort_append_chunks(list_, chunks[0])
    list_ = _helper_mapvote_convert_sort_append_chunks(list_, chunks[1][:-2])
    chunks[1][-1] = int(chunks[1][-1])
    list_.append(tuple(chunks[1][-2:]))
    return list_


class MapvoteChunk(object):
    @staticmethod
    def _is_mapvote_keeptwo(line):
        return _mapvote_keeptwo_regex.match(line)

    @staticmethod
    def _is_mapvote_finished(line):
        return _mapvote_finished_regex.match(line)

    @staticmethod
    def _get_mapvote_keeptwo_results(match):
        res = []
        if match:
            chunks = _helper_mapvote_split_match_chunk(match.group(1))
            res.append((chunks[0][0], int(chunks[0][1])))
            chunks[0] = chunks[0][2:]
            res = _helper_mapvote_append_chunks(res, chunks)
        return res

    @staticmethod
    def _get_mapvote_finished_results(match):
        res = []
        if match:
            chunks = _helper_mapvote_split_match_chunk(match.group(1))
            res = _helper_mapvote_append_chunks(res, chunks)
        return res

    @staticmethod
    def hook_name():
        return ['mapvote_finished', 'mapvote_keeptwo']

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_mapvote_keeptwo(line)
        if tmp:
            res = self._get_mapvote_keeptwo_results(tmp)
            caller.invoke_hooks('mapvote_keeptwo', res)
        tmp = self._is_mapvote_finished(line)
        if tmp:
            res = self._get_mapvote_finished_results(tmp)
            caller.invoke_hooks('mapvote_finished', res)
        return tmp
