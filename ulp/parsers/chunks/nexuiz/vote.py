import re
from ulp.helpers.nexuiz import convert_default_entries

_vcall_regex = re.compile(r'^(?:\s|\^7)?:vote:vcall:(?P<playerid>\d+?)'
                          r':(?P<vote>.+)$')
_vote_result_regex = re.compile(r'^(?:\s|\^7)?:vote'
                                r':v(?P<result>yes|no|timeout):(?P<yes>\d+):'
                                r'(?P<no>\d+):(?P<abstain>\d+):'
                                r'(?P<no_vote>\d+):(?P<minimum>\-?\d+)$')
_vote_stop_regex = re.compile(r'^(?:\s|\^7)?:vote:vstop:(?P<playerid>\d+)$')
_vote_login_regex = re.compile(r'^(?:\s|\^7)?:vote:vlogin:(?P<playerid>\d+)$')
_vote_do_regex = re.compile(r'^(\s|\^7)?:vote:vdo:(?P<playerid>\d+)'
                            r':(?P<do>.+)$')


class VoteChunk(object):
    @staticmethod
    def _is_vcall(line):
        return _vcall_regex.match(line)

    @staticmethod
    def _get_vcall_results(match):
        if match:
            return convert_default_entries(match.groupdict())

    @staticmethod
    def _is_vote_result(line):
        return _vote_result_regex.match(line)

    @staticmethod
    def _get_vote_result_results(match):
        if match:
            res = match.groupdict()
            for key in res.keys():
                if key != 'result':
                    res[key] = int(res[key])
            res['no vote'] = res['no_vote']
            del res['no_vote']
            return res

    @staticmethod
    def _is_vote_stop(line):
        return _vote_stop_regex.match(line)

    @staticmethod
    def _get_vote_stop_results(match):
        if match:
            return convert_default_entries(match.groupdict())

    @staticmethod
    def _is_vote_login(line):
        return _vote_login_regex.match(line)

    @staticmethod
    def _get_vote_login_results(match):
        if match:
            return convert_default_entries(match.groupdict())

    @staticmethod
    def _is_vote_do(line):
        return _vote_do_regex.match(line)

    @staticmethod
    def _get_vote_do_results(match):
        if match:
            return convert_default_entries(match.groupdict())

    @staticmethod
    def hook_name():
        return ['vcall', 'vote_do', 'vote_login', 'vote_result', 'vote_stop']

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_vcall(line)
        if tmp:
            res = self._get_vcall_results(tmp)
            caller.invoke_hooks('vcall', res)
            return tmp

        tmp = self._is_vote_result(line)
        if tmp:
            res = self._get_vote_result_results(tmp)
            caller.invoke_hooks('vote_result', res)
            return tmp

        tmp = self._is_vote_stop(line)
        if tmp:
            res = self._get_vote_stop_results(tmp)
            caller.invoke_hooks('vote_stop', res)
            return tmp

        tmp = self._is_vote_login(line)
        if tmp:
            res = self._get_vote_login_results(tmp)
            caller.invoke_hooks('vote_login', res)
            return tmp

        tmp = self._is_vote_do(line)
        if tmp:
            res = self._get_vote_do_results(tmp)
            caller.invoke_hooks('vote_do', res)
            return tmp
