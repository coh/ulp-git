import re

_gamestart_regex = re.compile(r'^(?:\s|\^7)?:gamestart:(?P<type>.+?)_'
                              r'(?P<map>.*):(?P<id>.*)$')


class GamestartChunk(object):
    @staticmethod
    def _is_gamestart(line):
        return _gamestart_regex.match(line)

    @staticmethod
    def _get_gamestart_results(match):
        if match:
            return match.groupdict()

    @staticmethod
    def hook_name():
        return 'gamestart'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_gamestart(line)
        if tmp:
            res = self._get_gamestart_results(tmp)
            caller.invoke_hooks('gamestart', res)
        return tmp
