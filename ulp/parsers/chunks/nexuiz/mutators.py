import re

_gameinfo_mutators_regex = re.compile(r'^(?:\s|\^7)?:gameinfo:mutators:'
                                      r'LIST(?P<mutators>.*)$')


class MutatorsChunk(object):
    @staticmethod
    def _is_gameinfo_mutators(line):
        return _gameinfo_mutators_regex.match(line)

    @staticmethod
    def _get_gameinfo_mutator_results(match):
        if match:
            res = match.groupdict(None)
            if len(res['mutators']):
                res['mutators'] = [x for x in res['mutators'].split(':') if x]
            else:
                res['mutators'] = None
            return res

    @staticmethod
    def hook_name():
        return 'mutators'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_gameinfo_mutators(line)
        if tmp:
            res = self._get_gameinfo_mutator_results(tmp)
            caller.invoke_hooks('mutators', res)
        return tmp
