import re

_recordset_regex = re.compile(r'^(?:\s|\^7)?:recordset:(?P<playerid>\d+)'
                              r':(?P<time>.+)$')


class RecordsetChunk(object):
    @staticmethod
    def _is_recordset(line):
        return _recordset_regex.match(line)

    @staticmethod
    def _get_recordset_results(match):
        if match:
            res = match.groupdict()
            res['playerid'] = int(res['playerid'])
            res['time'] = round(float(res['time']), 2)
            return res

    @staticmethod
    def hook_name():
        return 'recordset'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_recordset(line)
        if tmp:
            res = self._get_recordset_results(tmp)
            caller.invoke_hooks('recordset', res)
        return tmp
