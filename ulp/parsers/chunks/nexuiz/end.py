import re

_end_regex = re.compile(r'^(?:\s|\^7)?:end$')


class EndChunk(object):
    @staticmethod
    def _is_end(line):
        return _end_regex.match(line)

    @staticmethod
    def hook_name():
        return 'end'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_end(line)
        if tmp:
            caller.invoke_hooks('end', [])
        return tmp
