import re
from ulp.helpers.nexuiz import convert_default_entries

_dom_regex = re.compile(r'^(?:\s|\^7)?:dom:taken:(?P<previous_team>\d+)'
                        r':(?P<playerid>\d+)$')


class DomChunk(object):
    @staticmethod
    def _get_dom_results(match):
        if match:
            return convert_default_entries(
                match.groupdict(),
                {'previous_team': int}
            )

    @staticmethod
    def _is_dom(line):
        return _dom_regex.match(line)

    @staticmethod
    def hook_name():
        return 'dom_taken'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_dom(line)
        if tmp:
            res = self._get_dom_results(tmp)
            caller.invoke_hooks('dom_taken', res)
        return tmp
