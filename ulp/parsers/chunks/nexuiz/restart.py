import re

_restart_regex = re.compile(r'^(?:\s|\^7)?:restart$')


class RestartChunk(object):
    @staticmethod
    def _is_restart(line):
        return _restart_regex.match(line)

    @staticmethod
    def hook_name():
        return 'restart'

    @staticmethod
    def parse_priority():
        return 1

    def match_line(self, caller, line):
        tmp = self._is_restart(line)
        if tmp:
            caller.invoke_hooks('restart', [])
        return tmp
