def make_dict(keys, values):
    return dict(zip(keys, values))


def add_obj_attrs(obj, kwargs):
    for key, value in kwargs.items():
        setattr(obj, key, value)
