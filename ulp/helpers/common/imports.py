from __future__ import print_function

import glob
import sys
import os.path


def import_dir(dirname, scanner=None, pred=None):
    files = []

    def _custom_import(name):
        try:
            __import__(name)
        except ImportError as e:
            print('failed to import %s: %s' % (name, e), file=sys.stderr)
            return None
        return sys.modules[name]

    for item in glob.iglob(os.path.join(dirname, '*.py')):
        if item.find('__init__.py') == -1:
            #not found
            files.append(item.replace('.py', '').replace('/', '.'))

    files = [_custom_import(x) for x in files]

    if scanner and hasattr(scanner, '__call__'):
        if pred and hasattr(pred, '__call__'):
            files = [scanner(x) for x in files if pred(x)]
        else:
            files = [scanner(x) for x in files]

    return files


def import_classes(dirname, classtype=object, attribute=None):
    files = import_dir(dirname)
    classes = []

    for fil in files:
        for j in dir(fil):
            obj = getattr(fil, j)
            if isinstance(obj, classtype) and hasattr(obj, '__class__'):
                if (attribute and hasattr(obj, attribute)) or not attribute:
                    classes.append(obj)

    return classes
