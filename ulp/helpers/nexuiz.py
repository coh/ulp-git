import re

_color_escape_regex = r'\^(\d|x[a-fA-F0-9]{3})'


def decolorize_string(thestring):
    """ remove all color escapes from a string
    """
    return re.sub(_color_escape_regex, '', thestring)


def name_to_utf8(thename):
    """ maps all special playername characters to xonotic's range, but keeps
        the normal ones from 0x20 to 0x7e.
    """
    from sys import version_info
    res = ''
    chrfunc = chr
    tmp = 0

    if version_info[:2] < (3, 0):
        chrfunc = unichr
        res = u''

    for _ in thename:
        tmp = ord(_)
        if tmp < 0x20 or 0x7e < tmp < 0x100:
            res += chrfunc(0xe000 + tmp)
        else:
            res += _
    return res


def convert_default_entries(thedict, custom_list=None):
    ''' convert some entries in the chunk result dicts to default types
    '''
    conv_list = {
        'flagcolor': int,
        'jointype': int,
        'playerid': int,
        'slot': int,
        'teamid': int,
        'team': int,
    }
    res = thedict

    if custom_list and isinstance(custom_list, dict):
        for key, value in custom_list.items():
            conv_list[key] = value

    for item, func in conv_list.items():
        try:
            res[item] = func(res[item])
        except (KeyError, TypeError, ValueError):
            pass
    return res
