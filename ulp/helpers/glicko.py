""" Implements the Glicko Rating System, whose Author placed it into the
Public Domain.

This implementation follows his recommendation to never let the RD fall below
a certain value, f.e. 30, which is what we're using.
"""
import math

#ln(10)/400
_q = 0.0057565


def _new_rd(RD_old, c, t):
    return max(30.0, min(math.sqrt(pow(RD_old, 2) + pow(c, 2) * t), 350))


def _g(RD):
    return 1.0 / math.sqrt(1 + 3 * pow(_q, 2) * pow(RD, 2) / pow(math.pi, 2))


def _E(r_self, r_other, RD):
    return 1.0 / (1.0 + 1.0 / pow(10.0, _g(RD) * (r_self - r_other) / 400.0))


def _d2(r_self, results):
    thesum = 0.0
    for j in results:
        expect = _E(r_self, j[1], j[2])
        thesum += pow(_g(j[2]), 2) * expect * (1.0 - expect)
    return 1.0 / (pow(_q, 2) * thesum)


def glicko(player, results, c=63.2, t=1):
    r = 1500
    RD = 350

    if player:
        (r, RD) = player
        RD = _new_rd(RD, c, t)

    if results:
        r_dash = (_q / (1.0 / pow(RD, 2) + 1.0 / _d2(r, results)))
        thesum = 0.0
        for j in results:
            thesum += _g(j[2]) * (j[0] - _E(r, j[1], j[2]))
        new_r = round(r + r_dash * thesum)
        new_rd = round(math.sqrt(1.0 / (1.0 / pow(RD, 2) + 1.0 /
                                        _d2(r, results))), 1)
        return (new_r, new_rd)
    else:
        return (r, RD)
