from __future__ import print_function, with_statement

import sys


def _parse(indata, parsers):
    for line in indata:
        for parser in parsers:
            parser(line)


def parse(config_module=None):
    parsers = []
    source = None
    if not config_module and len(sys.argv) < 2:
        print("no config file given. aborting.")
        sys.exit(1)
    elif not config_module:
        config_module = sys.argv[1]
    # import the parser config
    try:
        __import__(config_module)
        module = sys.modules[config_module]
        if 'parser_list' in dir(module):
            parsers = module.parser_list()
        elif 'parsers' in dir(module):
            parsers = module.parsers
        else:
            raise ImportError("import failed due to an unknown reason.\n")
        if 'source' in dir(module):
            source = module.source
    except ImportError as e:
        print("failed to import config in %s. aborting." % config_module)
        print("Exception message is: %s" % e)
        sys.exit(1)
    except KeyError as e:
        print("failed to use given config in %s (keyerror). aborting." %
              config_module)
        print("Exception message is: %s" % e)
        sys.exit(1)

    if source:
        _parse(source, parsers)
    else:
        if len(sys.argv) > 2:
            with open(sys.argv[2]) as f:
                _parse(f, parsers)
        else:
            _parse(sys.stdin, parsers)


if __name__ == '__main__':
    try:
        parse()
    except KeyboardInterrupt:
        pass
