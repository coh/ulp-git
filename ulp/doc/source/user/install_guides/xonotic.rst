*************************
Install Guide for Xonotic
*************************

Prerequisites
=============

You'll need:

* `Xonotic <http://www.xonotic.org>`_
* `ulp <https://bitbucket.org/coh/ulp/>`_
* `Python <http://www.python.org>`_ >= 2.6a1
* easy_install or `pip <http://pip.openplans.org/>`_

Configure Xonotic
=================

In order to create the eventlog output, use this either in the server-console
or in the server config::

    sv_eventlog 1

Configure ulp
=============

Create or modify a config file in the ulp/config directory. The file format is
simple -- it is actual python code, so make sure to use 4 spaces instead of tab.

.. _test-your-configuration:

Test your configuration
=======================

If you've got a logfile at your hands, then just run

.. code-block:: bash

   python -m ulp.parse ulp.config.<configname_without_the_extension> <path_to_logfile>

or

.. code-block:: bash

   cat <logfile> | python -m ulp.parse ulp.config.<configname_without_the_extension>

Install ulp
===========

Once you're happy with your config, just run something like

.. code-block:: bash

   python setup.py install

to install ulp. You can also use a virtualenv to install ulp into.

Two ways to use ulp with Xonotic [#note1]_
============================================

We assume that, at this point, you've already installed ulp to a location where python will find it.

1. Parse a logfile
--------------------

First of all, this *can* work the very same way as descibed in the above 
:ref:`test-your-configuration`, but one can also apply more complicated means to that, such
as

.. code-block:: bash

   tail -f ~/.xonotic/data/games.log | python -m ulp.parse ulp.config.<configname_without_the_extension>

or

.. code-block:: bash

   ssh me@where.the.server.runs "tail -f <path_to_logfile>" | python -m ulp.parse ulp.config.<configname_without_the_extension>

As you can see, it's pretty unlimited of what you can achieve that way.

2. Receive the data through UDP
-------------------------------

Darkplaces gives you an option to send the (event-)log through UDP packets, so
let's get down to setting it up.

first off, enter this into the server console or config:

.. code-block:: bash

   log_dest_udp "ip[:port]"

where ip is the server ip where you want to run ulp and port defaults to 26000.

Of course, there's two warnings here:

* .. IMPORTANT::
     
     You can **NOT** use DNS names. You can **ONLY** use raw IP adresses or
     non-DNS names.

* .. WARNING::
     
     UDP packets can and *will* be lost, so some of the parser sinks may show up
     glitches. You've been warned, after all.

Since we want to receive the data over UDP, we need to create a new
``UdpSource`` instance and add it to the ``sources`` list::

    from ulp.sources.nexuiz.udp import UdpSource
    ...
    source = [UdpSource(),]

given that configuration change, we can now run it the very same way as
before:

.. code-block:: bash

   python -m ulp.parse ulp.config.<configname_without_the_extension>



.. rubric:: Footnotes

.. [#note1] A third one's in planning, so we'll handle it like the Spanish
   Inquisition...


