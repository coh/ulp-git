Known Issues
============

Nexuiz
******

* Without character re-encoding, the parser or sinks will fail.
  That is due to the way nexuiz encodes the special characters in player names.
* iconv eats up a huge amount of RAM during the charset conversion.
* the Sql logger in ``ulp/sinks/nexuiz/sql_logger.py`` is far from completion
  and doesn't work at all at the moment.

Xonotic
*******

* the Sql logger for xonotic isn't logging score results at the end of rounds
  yet.
* Several tables lack a datetime for events.

Nexuiz & Xonotic
****************

Several event types aren't logged yet, f. e. :

* ``part`` isn't logged to ``RoundLeave``
* Keyhunt events
