Glossary
========

.. glossary::

   Sink
      Data consumer who will 'consume' (i.e. use) the data provided by the 
      parser
   
   Source
      Data source for the parser
   

