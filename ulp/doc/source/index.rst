.. ulp documentation master file, created by
   sphinx-quickstart on Sat May 29 19:05:45 2010.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ulp's documentation!
===============================

ulp is a tool(-set) written in python allowing one to parse (game-)log files.

Contents:

.. toctree::
   :glob:
   :maxdepth: 2

   Changelog
   Contributors
   FAQ
   Glossary
   Known_issues
   LICENSE
   README
   dev/index
   user/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

