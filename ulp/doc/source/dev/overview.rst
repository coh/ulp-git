Graphical overview of ulp's structure
=====================================

.. blockdiag::

   diagram ulp_overview {
        InputData [color="#7f7f7f"];
        Source [color="#7f7f7f"];
        "parse.py" [color="#7f7f7f"];
        ChunkedParser [color="#7f7f7f"];
        Chunk_1 [color="#7f7f7f"];
        Chunk_2 [color="#7f7f7f"];
        Chunk_N [color="#7f7f7f"];
        "<game>Parser" [color="#7f7f7f"];
        Sink_1 [color="#7f7f7f"];
        Sink_2 [color="#7f7f7f"];
        Sink_N [color="#7f7f7f"];
        OutputData [color="#7f7f7f"];

        InputData -> "has data that are read by" -> Source -> "provides data to
        " -> "parse.py";
        "parse.py" -> "calls" -> Sink_1;
        "parse.py" -> "uses";
        "uses" -> "<game>Parser";
        "uses" -> ChunkedParser;
        ChunkedParser -> "is using" -> Chunk_1;
        "is using" -> Chunk_2;
        "is using" -> Chunk_N;
        "<game>Parser" -> "may returns a result to";
        ChunkedParser -> "may returns a result to";

        "may returns a result to" -> "parse.py";
        "calls" -> Sink_2;
        "calls" -> Sink_N;
        Sink_1 -> "gives you";
        Sink_2 -> "gives you";
        Sink_N -> "gives you";
        "gives you" -> OutputData;

        Source -> "can be one of";
        "can be one of" -> StdinSource;
        "can be one of" -> FileSource;
        "can be one of" -> UDPSource;

        group {
            label="sources/";
            color="#77ff77";
            UDPSource;
        }
        group {
            label="sinks/"
            color="#77ff77";
            Sink_1;
            Sink_2;
            Sink_N;
        }
        group {
            label="parsers/";
            color="#77ff77";
            "<game>Parser";
            ChunkedParser;
            "is using";
            group {
                label="parsers/chunks/<game>";
                Chunk_1;
                Chunk_2;
                Chunk_N;
            }
        }
   }

