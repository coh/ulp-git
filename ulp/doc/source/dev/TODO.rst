Things to do / Roadmap
======================

Immediately
***********

* Write the missing sqlalchemy insert commands/functions for the Xonotic sql_logger sink.

Very soon
*********

* Switch to py.test and either buildout, distribute or distutils2.

Soon
****

* Write the missing sqlalchemy insert commands/functions for the nexuiz sql_logger sink.

Near Future
***********

* Change from <other setup toolchain> to distutils2.

Future
******

* Add a backchannel source/sink combo.
* Add more rcon features.

