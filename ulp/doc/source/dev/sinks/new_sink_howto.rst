How to write a new Sink
=======================

The steps for writing a new sink are rather straightforward:

* Know what you want the sink to do
* Read up on the documentation for the parser in question regarding the hooks
* Create a new style class and implement the functionality
* add a specific method to the class

The following shows a simple example for a Sink that will count how often a map
in Nexuiz or Xonotic got loaded.

Know what you want the sink to do
*********************************

The Sink shall count how often a map got loaded.

Read up on the Parser's hook doc
********************************

The documentation gives us

   | 'gamestart':
   | ...
   | * 'map' the mapname

Create a new class and implement the functionality
**************************************************

A first version can look like this::

    from operator import itemgetter


    class MapPop(object):
        def __init__(self, filename):
            self._counts = {}
    
        def __del__(self):
            for name, count in sorted(self._counts.items(),
                key=itemgetter(1), reverse=True):
                print("%s\t%s" % (name, count))
    
        def gamestart(self, caller, data):
            try:
                self._counts[data['map']] += 1
            except KeyError:
                self._counts[data['map']] = 1

so we create a private dict ``_counts``. When we're done parsing the data,
``__del__`` is called and outputs the mapnames sorted by count in descending
order.

Add a specific method to the class
**********************************

This method makes sure that our ``gamestart`` function is actually called::

        def register_hooks(self, parser):
            parser.set_hook('gamestart', self.gamestart)




You're likely to use more than one hook in a Sink, which can be done this way::

    def register_hooks(self, parser):
        my_hooks = {
            'foo': self.foo,
            'bar': self.bar,
            ...
            'parrot': self.volts,
        }

        for hook, function in my_hooks.items():
            parser.set_hook(hook, function)



So the complete piece looks like this::
    
    from operator import itemgetter


    class MapPop(object):
        def __init__(self, filename):
            self._counts = {}
    
        def __del__(self):
            for name, count in sorted(self._counts.items(),
                key=itemgetter(1), reverse=True):
                print("%s\t%s" % (name, count))
    
        def gamestart(self, caller, data):
            try:
                self._counts[data['map']] += 1
            except KeyError:
                self._counts[data['map']] = 1

        def register_hooks(self, parser):
            parser.set_hook('gamestart', self.gamestart)



A more advanced version, which includes the ability to :py:mod:`python:pickle` the counts, can be found in ulp/examples/sinks/map_pop.py.
