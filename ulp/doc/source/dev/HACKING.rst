How to Contribute / Hacking ulp
===============================

Prerequisites
*************

You should be familiar with :pep:`8` and have already configured your editor
accordingly.

You need those packages for testing and building:

* coverage >= 3.4
* nose>=0.11.3
* Sphinx>=0.6.8
* SQLAlchemy>=0.6

Best install these through pip_, although easy_install_ should work as well.
It is generally advised against installing these packages (under Linux) through
the package installer, since most distros tend to ship somewhat outdated
versions of the above packages.

You may also need the Python development headers which (under Linux) are
typically in a package named python-devel, python-dev, or something similar.

.. _pip: http://pip.openplans.org/
.. _easy_install: http://pypi.python.org/pypi/setuptools

Developer commands
******************

* **python setup.py test**
  run the test suite
* **python setup.py nosetests**
  run the test suite, but with added code coverage
* **python setup.py build_sphinx**
  build the documentation.

Development workflow
********************

Fork on bitbucket, code along, make sure that you stay with latest revision(s).
When done, push out a pull request on bitbucket.

