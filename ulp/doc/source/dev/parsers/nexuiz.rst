*************************
The Nexuiz Chunked Parser
*************************

Important Caveat
================

.. ATTENTION::
   The name encoding for players in Nexuiz is a mixture of ascii for 0x20 to 0x7e,
   and 8-bit proprietary extensions based on gfx/conchars.tga [sic!]. Since some
   Python versions expect UTF-8 and that alone, you'll have to sanitize the
   logfile.

   You can take care of that by using something like this::

     cat <logfile> | iconv --from-code=ISO8859-1 --to-code=UTF-8 | <ulp>


Available Hooks
===============

* **<new_line>**
  invoked upon each log line
* **ctf_event**
  invoked upon steal/capture/fckill/return/drop of a flag in ctf game mode.
* **dom_taken**
  invoked when a point in domination game mode gets taken.
* **end**
* **gameover**
* **gamestart**
* **join_ai**
  invoked when a bot is joining the game
* **join_human**
  invoked when a human is joining the game
* **keyhunt**
  invoked when a keyhunt-specific event happens, such as f.e. key picked up.
* **kill_as**
  invoked on an accidential death or suicide
* **kill_ft**
  invoked on a frag or teamkill
* **label**
* **logversion**
  invoked on the beginning of a per-round eventlog file
* **mapvote_finished**
  invoked when the new map to be played has been determined.
* **mapvote_keeptwo**
  invoked when the map selection is down to two maps.
* **mutators**
  invoked upon round start.
* **name**
  invoked when a player/bot is changing his/her/its name
* **part**
  invoked when a player/bot is leaving
* **playerscore**
* **recordset**
  invoked when a new map record happend (f.e. new all-time fastest capture in
  ctf)
* **restart**
  invoked when the current round is restarted. this also applies to situations
  where a server has a warmup time before the actual round.
* **scores**
* **team**
  invoked when a player changed the team he's on
* **teamscore**
* **time**
  invoked whenever another event happend, but only in case of per-file 
  eventlogs
* **vcall**
  invoked whenever a vote call happens
* **vote_do**
  invoked in case of a vote_do vote
* **vote_login**
  invoked in case of a vote_login event
* **vote_result**
  invoked when a vote finished
* **vote_stop**
  invoked when a vote got aborted/stopped
* **vote_suggested**
  invoked when a map has been suggested as to be played next
* **vote_suggestion_accepted**
  invoked only if the suggested map will be played next.

Data-structure description for each given hook
==============================================

<new_line>
----------
the raw line as a string.

ctf_event
---------
a dictionary containing the following entries:

* **'event'**: *(string)* the type. can be one of

  - steal
  - dropped
  - pickup
  - capture
  - return
  - returned

* **'flagcolor'**: *(int)* the teamid
* **'playerid'**: *(int or None)* the playerid. *None* only in case of a returned event.

dom_taken
---------
a dictionary containing the following entries:

* **'previous_team'**: *(int)* the teamid of the previous team which had the node.
* **'playerid'**: *(int)* the playerid of the player who just captured the node.

end
---
an empty list.

gameover
--------
an empty list.

gamestart
---------
a dictionary containing the following entries:

* **'type'**: *(string)* the gametype, f.e. 'dm' or 'ctf'
* **'map'**: *(string)* the mapname
* **'id'**: *(string)* a (semi?-) unique round-id

join_ai
-------
a dictionary containing the following entries:

* **'human'**: *(False)* not a human player, thus *False*
* **'playerid'**: *(int)* the playerid
* **'slot'**: *(int)* the slot given to the player
* **'name'**: *(string)* the player name

join_human
----------
a dictionary containing the following entries:

* **'human'**: *(True)* a human player, thus *True*
* **'playerid'**: *(int)* the playerid
* **'slot'**: *(int)* the slot given to the player
* **'ip'**: *(string)* string containing an IPv4 address.
* **'name'**: *(string)* the player name

keyhunt
-------
a dictionary containing the following entries:

* **'eventtype'**: *(string)* one of

  - capture
  - carrierfrag
  - collect
  - destroyed
  - destroyed_holdingkey
  - dropkey
  - losekey
  - push
  - pushed

* **'playerid'**: *(int)* the playerid
* **'playerpoints'**: *(int)* the points for player with that playerid
* **'keyownerid'**: *(int)* the playerid for the player who has the key. 0 in case of no owner for that key
* **'keyownerpoints'**:  *(int)* the points for player with that playerid
* **'teamkey'**: *(string)* which/whose key

kill_as
-------
a dictionary containing the following entries:

* **'type'**: *(string)* one of

  - accident
  - suicide

* **'attackerid'**: *(int)* playerid of the attacker. should be the same value as for the victim, except maybe some special corner cases.
* **'victimid'**: *(int)* playerid of the victim. 
* **'cod'**: *(int)* cause of death. In case of value < 10000, it is an ORed combination of

  - **1** = **Laser**
  - **2** = **Shotgun**
  - **3** = **Uzi**
  - **4** = **Mortar**
  - **5** = **Electro**
  - **6** = **Crylink**
  - **7** = **Nex**
  - **8** = **Hagar**
  - **9** = **Rocket Launcher**
  - **10** = **Port-O-Launch**
  - **11** = **MinstaNex**
  - **12** = **Grappling Hook**
  - **13** = **Heavy Laser Assault Cannon**
  - **14** = **T.A.G. Seeker**

  and

  - **256** = **secondary fire**
  - **512** = **splash damage**
  - **1024** = **bounced projectile**
  - **2048** = **head shot (MinstaNex only)**
  - **4096** = **<unused flag>**

  Else, it is one of those:

  - **10000** = **fallen to death**
  - **10001** = **telefragged**
  - **10002** = **drowned**
  - **10003** = **killed by a trap / fallen into the void**
  - **10004** = **lava**
  - **10005** = **slime**
  - **10006** = **console kill**
  - **10007** = **(MinstaGib) out of ammo**
  - **10008** = **swamp**
  - **10009** = **team change**
  - **10010** = **auto team change**
  - **10011** = **camping protection**
  - **10012** = **player became too fast (should never happen)**
  - **10013** = **health rot**
  - **10014** = **mirror damage**
  - **10015** = **g_touchexplode**
  - **10100** = **turret**
  - **10150** = **spiderbot miniguns**
  - **10151** = **spiderbot rocket**
  - **10152** = **spiderbot, cushed by**
  - **10300** = **custom deathmessage**

* **'victimitems'**: *(dict)* with the following entries:
  
  - **'weapons'** *(int)* the OR'ed weapon-ids as described above
  - **'flags'** *(string)* with the following possible entries:

    + **F** = player carries the flag
    + **S** = player has strength
    + **I** = player has shield
    + **T** = player was typing
  
  - **'runes'** *(int)* the OR'ed rune/curse values as followed:

    + **1** = Strength
    + **2** = Defense
    + **4** = Regeneration
    + **8** = Speed
    + **16** = Vampire
    + **8192** = Weakness
    + **16384** = Vulnerability
    + **32768** = Venom
    + **65536** = Slow
    + **131072** = Empathy

kill_ft
-------
a dictionary containing the following entries:

* **'type'**: *(string)* see kill_as for explanation
* **'attackerid'**: *(int)* see kill_as.victimid, but for the attacker's id.
* **'victimid'**: *(int)* see kill_as for explanation
* **'cod'**: *(int)* see kill_as for explanation
* **'attackeritems'**: *(string)* see victimitems for an explanation, but for the attacker instead.
* **'victimitems'**: *(string)* see kill_as for explanation

label
-----

logversion
----------
an *int* with the version number

mapvote_finished
----------------
a *list* containing pairs (*2-tuples*) in this order:

* first, the winning map
* then, all the follow-ups sorted descending by vote-count
* at last, the "didn't vote" result

mapvote_keeptwo
---------------
pretty much the same as with mapvote_finished, except that the first two entries are the two maps selected for the 'final vote'.

mutators
--------
either a *list* of *strings*, or None in case that there's no mutator.

name
----
a dictionary containing the following entries:

* **'playerid'**: *(int)* playerid of the player who is renaming him/her/itself
* **'newname'**: *(string)* the new name

part
----
a dictionary containing the key 'playerid' with an *int* value which is the playerid.

playerscore
-----------

recordset
---------
a dictionary containing the following entries:

* **'playerid'**: *(int)* the playerid of the person/entity which just set the record
* **'time'**: *(float)* the new time of the record in seconds.

restart
-------
empty *list*

scores
------

team
----
a dictionary containing the following entries:

* **'playerid'**: *(int)* the playerid
* **'team'**: *(int)* the new team
* **'jointype'**: (*int* or *None*) the reason/method of joining that team. Either *None* or one of

  - **1** = **connect**
  - **2** = **auto**
  - **3** = **manual**
  - **4** = **spectating**
  - **6** = **adminmove**

teamscore
---------

time
----
a valid *datetime.datetime* instance

vcall
-----
a dictionary containing the following entries:

* **'playerid'**: *(int)* the player id of the vote caller
* **'vote'**: *(string)* what the vote is about

vote_do
-------
a dictionary containing the following entries:

* **'playerid'**: *(int)* the playerid
* **'do'**: *(string)* the thing to do

vote_login
----------
a dictionary with the key 'playerid' and an *int*-value which is the playerid.

vote_result
-----------
a dictionary containing the following entries:

* **'result'**: *(string)* one of 'no', 'yes' or 'timeout'
* **'yes'**: *(int)* the number of yes votes
* **'no'**: *(int)* the number of no votes
* **'abstain'**: *(int)* the number of abstain votes
* **'no vote'**: *(int)* the number of players who did not vote
* **'minimum'**: *(int)*

vote_stop
---------
a dictionary with the key 'playerid' and an *int*-value which is the playerid.

vote_suggested
--------------
a dictionary containing the following entries:

* **'map'**: *(string)* the map which was suggested
* **'playerid'**: *(int)* the player id of the player who suggested the map.

vote_suggestion_accepted
------------------------
a dictionary containing the following entries:

* **'map'**: *(string)* the map which will now be played.

