Contributors
============

The ulp Authors
***************

* Clemens-O. Hoppe <coh@co-hoppe.net>
* Robert "termac" Mietrach
* Pat Regan <thehead@patshead.com>


Thanks to
*********

* [-z-] and Merlijn for providing some test data for the nexuiz/xonotic parser
  which I'd not been able to get myself.
* divVerent for answering corner-case questions
* Dokujisan for providing me with a big piece of test data
