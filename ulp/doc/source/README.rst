Unified Logfile Parser (ulp) README
===================================

Target Audience
***************

ulp is meant to be used for parsing game logfiles.

Invocation
**********

Usually, you invoke ulp like this::

  python -m ulp.parse <configfile> [logfile]

if no logfile is specified, then stdin will be read. Thus, one can pipe the
logfile into parse.py .
