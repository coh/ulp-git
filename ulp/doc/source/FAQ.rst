Frequently asked Questions
==========================

Is there a way to issue commands into a running game based upon parsed events?
******************************************************************************
Yes.

If the game supports a form of remote rcon, you can use that to issue the
commands.

Else, one could write an asynchronuous spawner source sink with stdin&stdout
redirects. Then, it is only a matter of passing that source to a sink.

Do you plan on simplifying this?
--------------------------------
Yes.


Is this project still active? I haven't seen updates in <timespan here>!
************************************************************************
Yes, yet I'm studying, so all the dev work from me is done in my spare time.


Can I parse logs from <game here> instead of Nexuiz/Xonotic?
************************************************************
Yes.

As long as the log format is raw (line-based) text, then it is a matter of 
writing a new parser and optionally (a) new sink(s).

Else, you would also have to adapt the parse.py script itself. I would like to
ask you to write a clean version (class, instantiate in your config,
load&execute in parser.py) instead of adding an if/else-nightmare to parser.py
itself.


