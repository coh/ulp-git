from __future__ import print_function
from ulp.parsers.chunked import ChunkedParser
from ulp.sinks.xonotic.sql_logger import SqlLogger

_p = ChunkedParser('ulp/parsers/chunks/nexuiz', 'ulp/parsers/chunks/xonotic')

sinks = [SqlLogger('sqlite:///eventlog.sqlite')]
for sink in sinks:
    sink.register_hooks(_p)

parsers = [_p.parse]
