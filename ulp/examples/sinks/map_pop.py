from __future__ import print_function, with_statement
from operator import itemgetter

import pickle
import sys


class MapPop(object):
    def __init__(self, filename):
        self._filename = filename
        self._counts = None
        try:
            with open(self._filename, 'rb') as infile:
                self._counts = pickle.load(infile)
        except (IOError, pickle.PickleError):
            self._counts = {}

    def __del__(self):
        self.print_stats()
        try:
            with open(self._filename, 'wb') as outfile:
                pickle.dump(self._counts, outfile)
        except (IOError, pickle.PickleError):
            print('failed to save map pop data', file=sys.stderr)

    def print_stats(self):
        for name, count in sorted(self._counts.items(),
                                  key=itemgetter(1), reverse=True):
            print("%s\t%s" % (name, count))

    def gamestart(self, caller, data):
        try:
            self._counts[data['map']] += 1
        except KeyError:
            self._counts[data['map']] = 1

    def alternative_gamestart_implementation(self, caller, data):
        self._counts[data['map']] = self._counts.get(data['map'], 0) + 1

    def register_hooks(self, parser):
        parser.set_hook('gamestart', self.gamestart)
