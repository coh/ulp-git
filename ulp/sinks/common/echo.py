from __future__ import print_function


class EchoSink(object):
    def __init__(self):
        pass

    def echo(self, caller, line):
        print(line, end='')

    def register_hooks(self, parser):
        parser.set_hook('<new_line>', self.echo)
