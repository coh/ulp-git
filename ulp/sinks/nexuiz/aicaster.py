""" example sink for nexuiz to illustrate how to use the hooks and
parsed data.
"""
from __future__ import print_function


class NexAICaster(object):
    def __init__(self):
        super(NexAICaster, self).__init__()
        self._slots = {}
        for i in range(0, 64):
            self._slots[i] = None

    def frag_or_tk(self, caller, res):
        att = self._slots[res['attackerid']]
        vict = self._slots[res['victimid']]
        weap = res['cod']

        if res['type'] == 'tk':
            print("%s grew tired of fragging the enemy so he turned his \
attention to his own team mates" % att)
            return

        if weap < 10000:
            weap = weap & 15

        if weap == 1:
            print("%s showed his new laserpointer to %s" % (att, vict))
        elif weap == 2:
            print("%s wanted more pepper from %s and got it" % (vict, att))
        elif weap == 3:
            print("%s made %s feel like a swiss cheese" % (att, vict))
        elif weap == 4:
            print("%s gave %s some pills. they made him really red and sick."
                  % (att, vict))
        elif weap == 5:
            print("%s told %s to not to play with electricity, but he \
didn\'t listen." % (att, vict))
        elif weap == 6:
            print("%s killed %s with his flashlight" % (att, vict))
        elif weap == 7:
            print("%s demonstrated to %s what eagle-eye means" % (att, vict))
        elif weap == 9:
            print("%s died from the looks of %s\'s hard, long, fast... \
rocket" % (vict, att))
        else:
            print("%s fragged %s" % (att, vict))

    def rename(self, caller, res):
        oldname = self._slots[res['playerid']]
        self._slots[res['playerid']] = res['newname']
        print("%s changed the name to %s" % (oldname, res['newname']))

    def join_event(self, caller, res):
        self._slots[res['playerid']] = res['name']
        print("%s joined the fun." % res['name'])

    def join_ai(self, caller, res):
        self._slots[res['playerid']] = res['name']
        print("skynet spawned %s." % res['name'])

    def leave(self, caller, res):
        print("%s wanted to cuddle instead and so he \
left" % self._slots[res['playerid']])
        self._slots[res['playerid']] = None

    def register_hooks(self, parser):
        parser.set_hook('kill_ft', self.frag_or_tk)
        parser.set_hook('name', self.rename)
        parser.set_hook('join_ai', self.join_ai)
        parser.set_hook('join_human', self.join_event)
        parser.set_hook('part', self.leave)
