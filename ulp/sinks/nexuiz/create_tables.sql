-- The Number 64 is used throughout this document since that's the maximum
-- possible length for player- and map-names
create table Player (
  id INTEGER PRIMARY KEY
);

create table PlayerName (
  id INTEGER PRIMARY KEY,
  player_id INTEGER REFERENCES Player(id),
  name VARCHAR(64)
);

create table Mapname (
  id INTEGER PRIMARY KEY,
  name VARCHAR(64)
);

create table Mapvote (
  id INTEGER PRIMARY KEY,
  didnt_vote SMALLINT NULL,
  winner INTEGER REFERENCES MapvoteItem(id),
  had_keeptwo BOOLEAN DEFAULT FALSE
);

create table MapvoteItem (
  id INTEGER PRIMARY KEY,
  mapvote_id INTEGER REFERENCES Mapvote(id),
  map_id INTEGER REFERENCES Mapname(id),
  vote_count SMALLINT NULL
);

create table Mutator (
  id INTEGER PRIMARY KEY,
  name VARCHAR(64)
);

create table Round (
  id INTEGER PRIMARY KEY,
  began TIMESTAMP NULL,
  ended TIMESTAMP NULL,
  map_id INTEGER REFERENCES Mapname(id),
  round_type VARCHAR(64),
  unid VARCHAR(64) UNIQUE, -- kind of a uuid for the round
  restarted BOOLEAN DEFAULT FALSE
);

create table RoundMutator (
  id INTEGER PRIMARY KEY,
  round_id INTEGER REFERENCES Round(id),
  mutator_id INTEGER REFERENCES Mutator(id)
);

create table KillType (
  id INTEGER PRIMARY KEY,
  name VARCHAR(64)
);

create table KillEvent (
  id INTEGER PRIMARY KEY,
  kill_type SMALLINT REFERENCES KillType(id),
  attacker_id INTEGER REFERENCES Player(id) NULL,
  victim_id INTEGER REFERENCES Player(id),
  cause_of_death INTEGER
);

create table PlayerItems (
  id INTEGER PRIMARY KEY,
  player_id INTEGER REFERENCES Player(id),
  kill_id INTEGER REFERENCES KillEvent(id),
  weapons INTEGER,
  flag BOOLEAN DEFAULT FALSE,
  strength BOOLEAN DEFAULT FALSE,
  shield BOOLEAN DEFAULT FALSE,
  typing BOOLEAN DEFAULT FALSE,
  runes INTEGER NULL DEFAULT NULL
);


create table TeamColor (
  id SMALLINT PRIMARY KEY,
  colorname VARCHAR(64)
);


create table CtfEventType (
  id SMALLINT PRIMARY KEY,
  event_name VARCHAR(64)
);


create table CtfEvent (
  id INTEGER PRIMARY KEY,
  event SMALLINT REFERENCES CtfEventType(id),
  flagcolor SMALLINT REFERENCES TeamColor(id),
  playerid INTEGER REFERENCES Player(id) NULL
);

create table DomEvent (
  id INTEGER PRIMARY KEY,
  player_id INTEGER REFERENCES Player(id),
  previous_team SMALLINT REFERENCES TeamColor(id)
);

create table TeamJoinType (
  id SMALLINT PRIMARY KEY NULL,
  name VARCHAR(64)
);


create table TeamJoin (
  id INTEGER PRIMARY KEY,
  player_id INTEGER REFERENCES Player(id),
  round_id INTEGER REFERENCES Round(id),
  team SMALLINT REFERENCES TeamColor(id),
  jointype SMALLINT REFERENCES TeamJoinType(id) DEFAULT 0
);

create table Record (
  id INTEGER PRIMARY KEY,
  player_id INTEGER REFERENCES Player(id),
  record_time FLOAT
);

create table RoundJoin (
  id INTEGER PRIMARY KEY,
  human BOOLEAN,
  player_id INTEGER REFERENCES Player(id),
  nex_player_id INTEGER,
  round_id INTEGER REFERENCES Round(id),
  slot SMALLINT,
  ip VARCHAR(39) DEFAULT '0.0.0.0' -- maximum length of an IPv6 address
);

create table RoundLeave (
  id INTEGER PRIMARY KEY,
  player_id INTEGER REFERENCES Player(id),
  round_id INTEGER REFERENCES Round(id)
);

create table KeyhuntEventType (
  id SMALLINT PRIMARY KEY,
  name VARCHAR(64)
);


create table KeyhuntEvent (
  id INTEGER PRIMARY KEY,
  event_type SMALLINT REFERENCES KeyhuntEventType(id),
  player_id INTEGER REFERENCES Player(id),
  playerpoints INTEGER NULL,
  keyownerid INTEGER REFERENCES Player(id), -- no default of NULL, but allow it
  keyownerpoints INTEGER NULL,
  teamkey SMALLINT REFERENCES TeamColor(id)
);

create table VoteCall (
  id INTEGER PRIMARY KEY,
  player_id INTEGER REFERENCES Player(id), -- the vote caller
  vote VARCHAR(256), -- ought to be enough for every vote
  is_vote_do BOOLEAN DEFAULT FALSE
);

create table VoteLogin (
  id INTEGER PRIMARY KEY,
  player_id INTEGER REFERENCES Player(id)
);

create table VoteResultType (
  id SMALLINT PRIMARY KEY,
  vote_result VARCHAR(64)
);

create table VoteResult (
  id INTEGER PRIMARY KEY,
  vote_result SMALLINT REFERENCES VoteResultType(id),
  vote_id INTEGER REFERENCES VoteCall(id),
  yes_count INTEGER NULL,
  no_count INTEGER NULL,
  abstain_count INTEGER NULL,
  no_vote INTEGER NULL,
  minimum INTEGER NULL
);

create table VoteStop (
  id INTEGER PRIMARY KEY,
  stopper_id INTEGER REFERENCES Player(id),
  vote_id INTEGER REFERENCES VoteCall(id)
);

create table VoteMapSuggest (
  id INTEGER PRIMARY KEY,
  map_id INTEGER REFERENCES Mapname(id),
  player_id INTEGER REFERENCES Player(id),
  accepted BOOLEAN DEFAULT FALSE
);


