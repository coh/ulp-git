from __future__ import print_function, with_statement
import datetime
import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import ulp.sinks.xonotic.sql_tables as T

_DEBUG = True
LOG_FILENAME = 'logger.log'
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG if _DEBUG
                    else logging.ERROR)

_nonteam_modes = (
    'dm',
    'rune',
)


class SqlLogger(object):
    class LoggerState(object):
        def __init__(self):
            self._db_engine = None
            self._map = None
            self._round = None
            self._session_object = None
            self._session = None
            self._vote_had_keeptwo = None
            self._curr_mapvote = None
            self._vote = None
            self._vote_suggestion = None

        @property
        def db_engine(self):
            return self._db_engine

        @db_engine.setter
        def db_engine(self, new_engine):
            self._db_engine = new_engine

        @property
        def map(self):
            return self._map

        @map.setter
        def map(self, newmap):
            self._map = newmap

        @property
        def round(self):
            return self._round

        @round.setter
        def round(self, newround):
            self._round = newround

        @property
        def session(self):
            return self._session

        @session.setter
        def session(self, newsession):
            self._session = newsession

        @property
        def session_object(self):
            return self._session_object

        @session_object.setter
        def session_object(self, newsession_object):
            self._session_object = newsession_object

        @property
        def vote_had_keeptwo(self):
            return self._vote_had_keeptwo

        @vote_had_keeptwo.setter
        def vote_had_keeptwo(self, newvalue):
            self._vote_had_keeptwo = newvalue

        @property
        def curr_mapvote(self):
            return self._curr_mapvote

        @curr_mapvote.setter
        def curr_mapvote(self, newvote):
            self._curr_mapvote = newvote

        @property
        def vote(self):
            return self._vote

        @vote.setter
        def vote(self, newvote):
            self._vote = newvote

        @property
        def vote_suggestion(self):
            return self._vote_suggestion

        @vote_suggestion.setter
        def vote_suggestion(self, newsugg):
            self._vote_suggestion = newsugg

    def __init__(self, db_conn_string, echo_sql=False):
        self._state = SqlLogger.LoggerState()
        '''used to map playerid (xon) to T.Player.id (database)

        playerid (xon) is key, T.Player.id is value.
        '''
        self._player_mappings = {}

        #create db engine
        self._state.db_engine = create_engine(db_conn_string, echo=echo_sql)

        #create tables if they don't exist already
        metadata = T.get_base().metadata
        metadata.create_all(self._state.db_engine)

        #create session
        self._state.session_object = sessionmaker(bind=self._state.db_engine)

        #so that we don't have to worry about the existence of
        #self._state.session later on.
        self._state.session = self._state.session_object()
        self._insert_default_values()

    def __del__(self):
        self._commit_objects()

    @staticmethod
    def _get_killtype_id(thetype):
        for x in T.get_defaults_mapping()[T.KillType]:
            if unicode(thetype) == x['name']:
                return x['id']
        return None

    def _insert_default_values(self):
        logging.debug("checking for default table values")
        defaults = T.get_defaults_mapping()
        defaults_to_be_commited = []

        for table, default_rows in defaults.items():
            thequery = self._state.session.query(table)
            for row in default_rows:
                if not thequery.filter_by(**row).count():
                    #unpack the values and let the constructor do its magic
                    defaults_to_be_commited.append(table(**row))
                    logging.debug("table %s: staging row for commit: %r"
                                  % (table.__tablename__, row))
                else:
                    logging.debug("table %s: row already present: %r"
                                  % (table.__tablename__, row))
        self._commit_objects(defaults_to_be_commited)
        logging.debug("default table values initialized")

    def _insert_map(self, mapname):
        themap = None
        thequery = self._state.session.query(T.Mapname.id). \
            filter(T.Mapname.name == mapname)

        if thequery.count():
            logging.debug("insert map: exists %s" % mapname)
            themap = self._state.session.query(T.Mapname).get(thequery[0][0])
        else:
            themap = T.Mapname(name=mapname)
            self._commit_objects(themap)
            logging.debug("insert map: new %s" % mapname)

        return themap

    def _get_playeritems(self, items):
        res = T.PlayerItems()
        flags = {
            'F': res.flag,
            'S': res.strength,
            'I': res.shield,
            'T': res.typing,
        }
        res.weapons = items['weapons']
        res.runes = items['runes']
        if items['flags']:
            for letter, param in flags.items():
                if letter in items['flags']:
                    param = True
        return res

    def _get_ctf_event_id(self, event_name):
        thequery = self._state.session.query(T.CtfEventType.id). \
            filter(T.CtfEventType.event_name == event_name)

        if thequery.count():
            return thequery[0][0]
        else:
            return -1

    def _get_mutator_id_by_name(self, mutatorname):
        thequery = self._state.session.query(T.Mutator.id). \
            filter(T.Mutator.name == mutatorname)

        if thequery.count():
            return thequery[0][0]
        else:
            themutator = T.Mutator()
            themutator.name = mutatorname
            self._commit_objects(themutator)
            return themutator.id

    def _get_player_id_for_name(self, playername):
        theplayername = None
        thequery = self._state.session.query(T.PlayerName.player_id). \
            filter(T.PlayerName.name == playername)

        if thequery.count():
            logging.debug("player name: exists %s" % playername)
            theplayername = self._state.session.query(T.PlayerName).get(
                thequery[0][0])
        else:
            theplayername = T.PlayerName()
            theplayer = T.Player()
            self._state.session.add(theplayer)
            self._state.session.commit()
            theplayername.name = playername
            theplayername.player_id = theplayer.id
            self._commit_objects(theplayername)
        return theplayername

    def _get_vote_result_type(self, result):
        thequery = self._state.session.query(T.VoteResultType.id). \
            filter(T.VoteResultType.vote_result == result)

        if thequery.count():
            return thequery[0]
        else:
            res = T.VoteResultType()
            res.id = -1
            return res

    def _add_objects(self, obj_or_objlist):
        if not isinstance(obj_or_objlist, (list, tuple)):
            obj_or_objlist = [obj_or_objlist, ]
        self._state.session.add_all(obj_or_objlist)

    def _commit_objects(self, obj_or_objlist=None):
        if obj_or_objlist:
            self._add_objects(obj_or_objlist)
        self._state.session.commit()

    def ctf_event(self, caller, data):
        thectfevent = T.CtfEvent()
        thectfevent.event = self._get_ctf_event_id(data['event'])
        thectfevent.flagcolor = data['flagcolor']
        thectfevent.round_id = self._state.round.id
        thectfevent.event_time = datetime.datetime.utcnow()
        if data['playerid']:
            thectfevent.player_id = self._player_mappings[data['playerid']]
        else:
            thectfevent.player_id = -1
        self._add_objects(thectfevent)

    def dom_taken(self, caller, data):
        thedom = T.DomEvent()
        thedom.round_id = self._state.round.id
        thedom.player_id = self._player_mappings[data['playerid']]
        thedom.previous_team = data['previous_team']
        thedom.event_time = datetime.datetime.utcnow()
        self._add_objects(thedom)

    def gamestart(self, caller, data):
        logging.debug("gamestart data: %s" % data)
        #do the mapname portion
        themap = self._insert_map(data['map'])
        #and with that, do the actual gamestart entry
        theround = T.Round()
        theround.map_id = themap.id
        theround.round_type = data['type']
        theround.unid = data['id']
        theround.began = datetime.datetime.utcnow()

        self._state.round = theround
        self._commit_objects(theround)

    def gameover(self, caller, data):
        logging.debug("gameover data: %s" % data)
        if not self._state.round:
            return
        self._state.round.ended = datetime.datetime.utcnow()
        self._state.session.commit()

    def join(self, caller, data):
        if not self._state.round:
            return
        thejoin = T.RoundJoin()
        thejoin.human = data['human']
        thejoin.nex_player_id = data['playerid']
        thejoin.player_id = self._get_player_id_for_name(data['name']).id
        self._player_mappings[thejoin.nex_player_id] = thejoin.player_id
        thejoin.slot = data['slot']
        thejoin.round_id = self._state.round.id
        thejoin.event_time = datetime.datetime.utcnow()
        if thejoin.human:
            thejoin.ip = data['ip']
        self._commit_objects(thejoin)

    def kill(self, caller, data):
        objs = []
        logging.debug("kill event data: %s" % data)
        killtype_id = self._get_killtype_id(data['type'])

        thekill = T.KillEvent()
        thekill.kill_type = killtype_id
        thekill.attacker_id = data["attackerid"]
        thekill.victim_id = data["victimid"]
        thekill.cause_of_death = data["cod"]
        thekill.round_id = self._state.round.id
        thekill.event_time = datetime.datetime.utcnow().time()
        objs.append(thekill)

        thevictimitems = self._get_playeritems(data['victimitems'])
        thevictimitems.player_id = self._player_mappings[data['victimid']]
        theattackeritems = None
        objs.append(thevictimitems)

        if data['type'] not in ('accident', 'suicide'):
            theattackeritems = self._get_playeritems(data['attackeritems'])
            theattackeritems.player_id = \
                self._player_mappings[data['attackerid']]
            self._player_mappings[data['attackerid']]
            objs.append(theattackeritems)

        self._commit_objects(objs)

        thevictimitems.kill_id = thekill.id
        if theattackeritems:
            theattackeritems.kill_id = thekill.id

    def mapvote_keeptwo(self, caller, data):
        self._state.vote_had_keeptwo = True
        self._state.curr_mapvote = data[2:-1]

    def mapvote_finished(self, caller, data):
        if self._state.vote_had_keeptwo:
            tmp = self._state.curr_mapvote[:]
            self._state.curr_mapvote = data[0:2] + tmp + [data[-1]]
        else:
            self._state.curr_mapvote = data

        thewinner = self._insert_map(self._state.curr_mapvote[0][0])
        themapvote = T.Mapvote()
        themapvote.vote_date = datetime.datetime.utcnow()
        themapvote.didnt_vote = self._state.curr_mapvote[-1][1]
        themapvote.winner = thewinner.id
        themapvote.had_keeptwo = self._state.vote_had_keeptwo
        self._commit_objects(themapvote)

        mapvote_items = []
        for mapname, vote_count in self._state.curr_mapvote[:-1]:
            themap = self._insert_map(mapname)
            themapvoteitem = T.MapvoteItem()
            themapvoteitem.mapvote_id = themapvote.id
            themapvoteitem.map_id = themap.id
            themapvoteitem.vote_count = vote_count
            mapvote_items.append(themapvoteitem)

        self._commit_objects(mapvote_items)
        self._state.curr_mapvote = None
        self._state.vote_had_keeptwo = False

    def mutators(self, caller, data):
        if data['mutators']:
            for mutator in data['mutators']:
                mutatorid = self._get_mutator_id_by_name(mutator)
                theroundmutator = T.RoundMutator()
                theroundmutator.round_id = self._state.round.id
                theroundmutator.mutator_id = mutatorid
                self._add_objects(theroundmutator)

    def part(self, caller, data):
        #TODO: log the part event into RoundLeave
        if data['playerid'] in self._player_mappings.keys():
            del self._player_mappings[data['playerid']]

    def playerscore(self, caller, data):
        #TODO
        pass

    def recordset(self, caller, data):
        therecord = T.Record()
        therecord.player_id = self._player_mappings.get(data['playerid'], -1)
        therecord.record_time = data['time']
        therecord.round_id = self._state.round.id
        therecord.event_time = datetime.datetime.utcnow()
        self._add_objects(therecord)

    def rename(self, caller, data):
        thequery = self._state.session.query(T.PlayerName). \
            filter(T.PlayerName.name == data['newname'])
        if not thequery.count():
            thename = T.PlayerName()
            thename.player_id = self._player_mappings[data['playerid']]
            thename.name = data['newname']
            self._add_objects(thename)

    def restart(self, caller, data):
        self._state.round.restarted = True

    def scores(self, caller, data):
        #TODO
        pass

    def team(self, caller, data):
        logging.debug("team data: %s" % data)
        theteamjoin = T.TeamJoin()
        theteamjoin.player_id = self._player_mappings.get(data['playerid'], -1)
        theteamjoin.round_id = self._state.round.id
        theteamjoin.event_time = datetime.datetime.utcnow()
        if self._state.round.round_type in _nonteam_modes:
            #in case of non team-based modes, teamid is actually
            #the player's shirt+pants color
            theteamjoin.team = 0
        else:
            theteamjoin.team = data['team']
        theteamjoin.jointype = data['jointype']
        self._commit_objects(theteamjoin)

    def teamscore(self, caller, data):
        #TODO
        pass

    def vcall(self, caller, data):
        thecall = T.VoteCall()
        try:
            thecall.player_id = self._player_mappings[data['playerid']]
        except KeyError:
            #this can happen due to a freak bug in old nexuiz
            thecall.player_id = 0
        thecall.round_id = self._state.round.id
        thecall.vote_time = datetime.datetime.utcnow()
        if 'do' in data.keys():
            thecall.vote = data['do']
            thecall.is_vote_do = True
        else:
            thecall.vote = data['vote']
        self._commit_objects(thecall)
        self._state.vote = thecall

    def vote_accepted(self, caller, data):
        self._state.vote_suggestion.accepted = True

    def vote_suggested(self, caller, data):
        themap = self._insert_map(data['map'])
        thevs = T.VoteMapSuggest()
        thevs.player_id = self._player_mappings[data['playerid']]
        thevs.round_id = self._state.round.id
        thevs.map_id = themap.id
        self._add_objects(thevs)
        self._state.vote_suggestion = thevs

    def vote_login(self, caller, data):
        thelogin = T.VoteLogin()
        thelogin.player_id = self._player_mappings[data['playerid']]
        thelogin.round_id = self._state.round.id
        thelogin.event_time = datetime.datetime.utcnow()
        self._add_objects(thelogin)

    def vote_result(self, caller, data):
        theresult = T.VoteResult()
        resulttype = self._get_vote_result_type(data['result'])
        theresult.vote_result = resulttype.id
        theresult.vote_id = self._state.vote.id
        theresult.yes_count = data['yes']
        theresult.no_count = data['no']
        theresult.abstain_count = data['abstain']
        theresult.no_vote = data['no vote']
        theresult.minimum = data['minimum']
        self._add_objects(theresult)

    def vote_stop(self, caller, data):
        thestop = T.VoteStop()
        thestop.stopper_id = self._player_mappings[data['playerid']]
        thestop.vote_id = self._state.vote.id
        self._add_objects(thestop)

    def register_hooks(self, parser):
        #'function pointer'
        hookdict = {
            'ctf_event': self.ctf_event,
            'dom_taken': self.dom_taken,
            'gameover': self.gameover,
            'gamestart': self.gamestart,
            'join_ai': self.join,
            'join_human': self.join,
            'kill_as': self.kill,
            'kill_ft': self.kill,
            'mapvote_finished': self.mapvote_finished,
            'mapvote_keeptwo': self.mapvote_keeptwo,
            'mutators': self.mutators,
            'name': self.rename,
            'part': self.part,
            'recordset': self.recordset,
            'restart': self.restart,
            'team': self.team,
            'vcall': self.vcall,
            'vote_do': self.vcall,
            'vote_login': self.vote_login,
            'vote_result': self.vote_result,
            'vote_suggested': self.vote_suggested,
            'vote_suggestion_accepted': self.vote_accepted,
        }

        for name, func in hookdict.items():
            parser.set_hook(name, func)
