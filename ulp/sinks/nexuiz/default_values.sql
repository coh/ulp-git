-- arbitrarily chosen id-values
INSERT INTO KillType (id, name) VALUES (1, 'accident');
INSERT INTO KillType (id, name) VALUES (2, 'frag');
INSERT INTO KillType (id, name) VALUES (4, 'suicide');
INSERT INTO KillType (id, name) VALUES (8, 'tk');


-- in case of not taken dom node
INSERT INTO TeamColor (id, colorname) VALUES (1, 'No team');
INSERT INTO TeamColor (id, colorname) VALUES (5, 'Red team');
INSERT INTO TeamColor (id, colorname) VALUES (14, 'Blue team');
INSERT INTO TeamColor (id, colorname) VALUES (13, 'Yellow team');
INSERT INTO TeamColor (id, colorname) VALUES (10, 'Pink team');


-- arbitrarily chosen id-values
INSERT INTO CtfEventType (id, event_name) VALUES (1, 'capture');
INSERT INTO CtfEventType (id, event_name) VALUES (2, 'dropped');
INSERT INTO CtfEventType (id, event_name) VALUES (4, 'pickup');
INSERT INTO CtfEventType (id, event_name) VALUES (8, 'return');
INSERT INTO CtfEventType (id, event_name) VALUES (16, 'returned');
INSERT INTO CtfEventType (id, event_name) VALUES (32, 'steal');


INSERT INTO TeamJoinType (id, name) VALUES(0,'None');
INSERT INTO TeamJoinType (id, name) VALUES(1,'connect');
INSERT INTO TeamJoinType (id, name) VALUES(2,'auto');
INSERT INTO TeamJoinType (id, name) VALUES(3,'manual');
INSERT INTO TeamJoinType (id, name) VALUES(4,'spectating');
INSERT INTO TeamJoinType (id, name) VALUES(6,'adminmove');


-- arbitrarily chosen id-values
INSERT INTO KeyhuntEventType (id, name) VALUES (1, 'capture');
INSERT INTO KeyhuntEventType (id, name) VALUES (2, 'carrierfrag');
INSERT INTO KeyhuntEventType (id, name) VALUES (3, 'collect');
INSERT INTO KeyhuntEventType (id, name) VALUES (4, 'destroyed');
INSERT INTO KeyhuntEventType (id, name) VALUES (5, 'destroyed_holdingkey');
INSERT INTO KeyhuntEventType (id, name) VALUES (6, 'dropkey');
INSERT INTO KeyhuntEventType (id, name) VALUES (7, 'losekey');
INSERT INTO KeyhuntEventType (id, name) VALUES (8, 'push');
INSERT INTO KeyhuntEventType (id, name) VALUES (9, 'pushed');


-- arbitrarily chosen id-values
INSERT INTO VoteResultType (id, vote_result) VALUES (1, 'no');
INSERT INTO VoteResultType (id, vote_result) VALUES (2, 'timeout');
INSERT INTO VoteResultType (id, vote_result) VALUES (4, 'yes');


