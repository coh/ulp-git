from ulp.helpers.common.utils import add_obj_attrs
from sqlalchemy import Boolean, Column, DateTime, Float, ForeignKey
from sqlalchemy import Integer, String, SmallInteger, Time

import sqlalchemy.ext.declarative as D


_Base = D.declarative_base()


def get_base():
    return _Base


def get_defaults_mapping():
    return _defaults_mapping


class Player(_Base):
    __tablename__ = 'player'
    id = Column(Integer, primary_key=True)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class PlayerName(_Base):
    __tablename__ = 'playername'
    id = Column(Integer, primary_key=True)
    player_id = Column(Integer, ForeignKey('player.id'))
    name = Column(String)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class Mapname(_Base):
    __tablename__ = 'mapname'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class Mapvote(_Base):
    __tablename__ = 'mapvote'
    id = Column(Integer, primary_key=True)
    didnt_vote = Column(SmallInteger)
    winner = Column(Integer, ForeignKey('mapname.id'))
    had_keeptwo = Column(Boolean, default=False)
    vote_date = Column(DateTime)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class MapvoteItem(_Base):
    __tablename__ = 'mapvoteitem'
    id = Column(Integer, primary_key=True)
    mapvote_id = Column(Integer, ForeignKey('mapvote.id'))
    map_id = Column(Integer, ForeignKey('mapname.id'))
    vote_count = Column(SmallInteger, default=0)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class Mutator(_Base):
    __tablename__ = 'mutator'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class Round(_Base):
    __tablename__ = 'round'
    id = Column(Integer, primary_key=True)
    began = Column(DateTime)
    ended = Column(DateTime)
    map_id = Column(Integer, ForeignKey('mapname.id'))
    round_type = Column(String)
    #unid = Column(String, unique=True)
    unid = Column(String)
    restarted = Column(Boolean, default=False)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class RoundMutator(_Base):
    __tablename__ = 'roundmutator'
    id = Column(Integer, primary_key=True)
    round_id = Column(Integer, ForeignKey('round.id'))
    mutator_id = Column(Integer, ForeignKey('mutator.id'))

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class KillType(_Base):
    __tablename__ = 'killtype'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class KillEvent(_Base):
    __tablename__ = 'killevent'
    id = Column(Integer, primary_key=True)
    kill_type = Column(Integer, ForeignKey('killtype.id'))
    attacker_id = Column(Integer, ForeignKey('player.id'))
    victim_id = Column(Integer, ForeignKey('player.id'))
    cause_of_death = Column(Integer)
    round_id = Column(Integer, ForeignKey('round.id'))
    event_time = Column(DateTime)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class PlayerItems(_Base):
    __tablename__ = 'playeritems'
    id = Column(Integer, primary_key=True)
    player_id = Column(Integer, ForeignKey('player.id'))
    kill_id = Column(Integer, ForeignKey('killevent.id'))
    weapons = Column(Integer)
    flag = Column(Boolean, default=False)
    strength = Column(Boolean, default=False)
    shield = Column(Boolean, default=False)
    typing = Column(Boolean, default=False)
    runes = Column(Integer, default=0)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class TeamColor(_Base):
    __tablename__ = 'teamcolor'
    id = Column(Integer, primary_key=True)
    colorname = Column(String)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class CtfEventType(_Base):
    __tablename__ = 'ctfeventtype'
    id = Column(Integer, primary_key=True)
    event_name = Column(String)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class CtfEvent(_Base):
    __tablename__ = 'ctfevent'
    id = Column(Integer, primary_key=True)
    event = Column(Integer, ForeignKey('ctfeventtype.id'))
    flagcolor = Column(Integer, ForeignKey('teamcolor.id'))
    round_id = Column(Integer, ForeignKey('round.id'))
    player_id = Column(Integer, ForeignKey('player.id'))
    event_time = Column(DateTime)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class DomEvent(_Base):
    __tablename__ = 'domevent'
    id = Column(Integer, primary_key=True)
    round_id = Column(Integer, ForeignKey('round.id'))
    player_id = Column(Integer, ForeignKey('player.id'))
    previous_team = Column(Integer, ForeignKey('teamcolor.id'))
    event_time = Column(DateTime)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class TeamJoinType(_Base):
    __tablename__ = 'teamjointype'
    id = Column(SmallInteger, primary_key=True)
    name = Column(String)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class TeamJoin(_Base):
    __tablename__ = 'teamjoin'
    id = Column(Integer, primary_key=True)
    player_id = Column(Integer, ForeignKey('player.id'))
    round_id = Column(Integer, ForeignKey('round.id'))
    team = Column(Integer, ForeignKey('teamcolor.id'))
    jointype = Column(SmallInteger, ForeignKey('teamjointype.id'))
    event_time = Column(DateTime)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class Record(_Base):
    __tablename__ = 'record'
    id = Column(Integer, primary_key=True)
    player_id = Column(Integer, ForeignKey('player.id'))
    round_id = Column(Integer, ForeignKey('round.id'))
    record_time = Column(Float)
    event_time = Column(DateTime)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class RoundJoin(_Base):
    __tablename__ = 'roundjoin'
    id = Column(Integer, primary_key=True)
    human = Column(Boolean)
    player_id = Column(Integer, ForeignKey('player.id'))
    nex_player_id = Column(Integer)
    round_id = Column(Integer, ForeignKey('round.id'))
    slot = Column(SmallInteger)
    ip = Column(String, default="0.0.0.0")
    event_time = Column(DateTime)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class RoundLeave(_Base):
    __tablename__ = 'roundleave'
    id = Column(Integer, primary_key=True)
    player_id = Column(Integer, ForeignKey('player.id'))
    round_id = Column(Integer, ForeignKey('round.id'))
    event_time = Column(DateTime)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class KeyhuntEventType(_Base):
    __tablename__ = 'keyhunteventtype'
    id = Column(SmallInteger, primary_key=True)
    name = Column(String)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class KeyhuntEvent(_Base):
    __tablename__ = 'keyhuntevent'
    id = Column(Integer, primary_key=True)
    event_type = Column(SmallInteger, ForeignKey('keyhunteventtype.id'))
    player_id = Column(Integer, ForeignKey('player.id'))
    playerpoints = Column(Integer, default=0)
    keyownerid = Column(Integer, ForeignKey('player.id'))
    keyownerpoints = Column(Integer, default=0)
    teamkey = Column(Integer, ForeignKey('teamcolor.id'))
    event_time = Column(DateTime)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class VoteCall(_Base):
    __tablename__ = 'votecall'
    id = Column(Integer, primary_key=True)
    player_id = Column(Integer, ForeignKey('player.id'))
    round_id = Column(Integer, ForeignKey('round.id'))
    vote = Column(String)
    is_vote_do = Column(Boolean, default=False)
    vote_time = Column(DateTime)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class VoteLogin(_Base):
    __tablename__ = 'votelogin'
    id = Column(Integer, primary_key=True)
    player_id = Column(Integer, ForeignKey('player.id'))
    round_id = Column(Integer, ForeignKey('round.id'))
    event_time = Column(DateTime)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class VoteResultType(_Base):
    __tablename__ = 'voteresulttype'
    id = Column(SmallInteger, primary_key=True)
    vote_result = Column(String)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class VoteResult(_Base):
    __tablename__ = 'voteresult'
    id = Column(Integer, primary_key=True)
    vote_result = Column(SmallInteger, ForeignKey('voteresulttype.id'))
    vote_id = Column(Integer, ForeignKey('votecall.id'))
    yes_count = Column(Integer, default=0)
    no_count = Column(Integer, default=0)
    abstain_count = Column(Integer, default=0)
    no_vote = Column(Integer, default=0)
    minimum = Column(Integer, default=0)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class VoteStop(_Base):
    __tablename__ = 'votestop'
    id = Column(Integer, primary_key=True)

    stopper_id = Column(Integer, ForeignKey('player.id'))
    vote_id = Column(Integer, ForeignKey('votecall.id'))

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


class VoteMapSuggest(_Base):
    __tablename__ = 'votemapsuggest'
    id = Column(Integer, primary_key=True)
    map_id = Column(Integer, ForeignKey('mapname.id'))
    player_id = Column(Integer, ForeignKey('player.id'))
    round_id = Column(Integer, ForeignKey('round.id'))
    accepted = Column(Boolean, default=False)

    def __init__(self, **kwargs):
        add_obj_attrs(self, kwargs)


_defaults_mapping = {
    # arbitrarily chosen id-values
    KillType: (
        {"id": 1, "name": unicode("accident")},
        {"id": 2, "name": unicode("frag")},
        {"id": 4, "name": unicode("suicide")},
        {"id": 8, "name": unicode("tk")},
    ),
    TeamColor: (
        # in case of not taken dom node
        {"id": 1, "colorname": unicode('No team')},
        {"id": 5, "colorname": unicode('Red team')},
        {"id": 14, "colorname": unicode('Blue team')},
        {"id": 13, "colorname": unicode('Yellow team')},
        {"id": 10, "colorname": unicode('Pink team')},
    ),
    # arbitrarily chosen id-values
    CtfEventType: (
        {"id": 1, "event_name": unicode('capture')},
        {"id": 2, "event_name": unicode('dropped')},
        {"id": 4, "event_name": unicode('pickup')},
        {"id": 8, "event_name": unicode('return')},
        {"id": 16, "event_name": unicode('returned')},
        {"id": 32, "event_name": unicode('steal')},
    ),
    TeamJoinType: (
        {"id": 0, "name": unicode('None')},
        {"id": 1, "name": unicode('connect')},
        {"id": 2, "name": unicode('auto')},
        {"id": 3, "name": unicode('manual')},
        {"id": 4, "name": unicode('spectating')},
        {"id": 6, "name": unicode('adminmove')},
    ),
    # arbitrarily chosen id-values
    KeyhuntEventType: (
        {"id": 1, "name": unicode('capture')},
        {"id": 2, "name": unicode('carrierfrag')},
        {"id": 3, "name": unicode('collect')},
        {"id": 4, "name": unicode('destroyed')},
        {"id": 5, "name": unicode('destroyed_holdingkey')},
        {"id": 6, "name": unicode('dropkey')},
        {"id": 7, "name": unicode('losekey')},
        {"id": 8, "name": unicode('push')},
        {"id": 9, "name": unicode('pushed')},
    ),
    # arbitrarily chosen id-values
    VoteResultType: (
        {"id": 1, "vote_result": unicode('no')},
        {"id": 2, "vote_result": unicode('timeout')},
        {"id": 4, "vote_result": unicode('yes')},
    ),
}
