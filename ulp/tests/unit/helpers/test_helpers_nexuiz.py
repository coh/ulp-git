import unittest
import ulp.helpers.nexuiz as N
from ulp.tests.utils import run_equal_asserts


class TestHelpersNexuiz(unittest.TestCase):
    def test_decolorize_string(self):
        asserts = {
            "^7^x112CU^x005|^4dyin^7^7 picked up the ^3yellow key":
            "CU|dyin picked up the yellow key",
            "foobar": "foobar"
        }

        run_equal_asserts(self, asserts, N.decolorize_string)
