import unittest
from ulp.helpers import glicko


class TestHelpersGlicko(unittest.TestCase):
    def test_glicko(self):
        own_skill = (1500, 200)
        results = [(1, 1400, 30), (0, 1550, 100), (0, 1700, 300)]
        new_skill = glicko.glicko(own_skill, results, t=0)
        self.assertEqual(new_skill, (1464, 151.4))
