import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.gameover as M


class TestGameoverChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.GameoverChunk()

    def test_is_gameover(self):
        asserts = {
            ":gameover": [],
            "^7:gameover": [],
            ":gameove": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'gameover')
