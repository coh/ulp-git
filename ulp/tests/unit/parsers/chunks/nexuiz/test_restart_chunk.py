import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.restart as M


class TestRestartChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.RestartChunk()

    def test_is_restart(self):
        asserts = {
            ":restart": [],
            "^7:restart": [],
            ":restar": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'restart')
