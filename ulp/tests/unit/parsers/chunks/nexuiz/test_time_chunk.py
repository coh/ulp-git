import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.time as M


class TestTimeChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.TimeChunk()

    def test_get_time(self):
        import datetime
        asserts = {
            ":time:2010-05-04 19:09:59":
            datetime.datetime(2010, 5, 4, 19, 9, 59),
            "^7:time:2010-05-04 19:25:51":
            datetime.datetime(2010, 5, 4, 19, 25, 51),
            ":time:2010-05-04 19:25": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'time')
