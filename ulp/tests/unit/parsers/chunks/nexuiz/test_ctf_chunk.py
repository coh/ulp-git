import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.ctf as M


class TestCtfChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.CtfChunk()

    def test_ctf_chunk(self):
        """ test if we're getting proper results for the ctf player regex
        """
        asserts = {
            "^7:ctf:capture:5:1": {
                'event': 'capture',
                'flagcolor': 5,
                'playerid': 1
            },
            "^7:ctf:dropped:14:1": {
                'event': 'dropped',
                'flagcolor': 14,
                'playerid': 1
            },
            "^7:ctf:steal:5:1": {
                'event': 'steal',
                'flagcolor': 5,
                'playerid': 1
            },
            "^7:ctf:returned:5": {
                'event': 'returned',
                'flagcolor': 5,
                'playerid': None
            },
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'ctf_event')
