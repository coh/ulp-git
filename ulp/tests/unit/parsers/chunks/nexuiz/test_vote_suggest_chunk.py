import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.vote_suggest as M


class TestVoteSuggestChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.VoteSuggestChunk()

    def test_get_vote_suggested_results(self):
        asserts = {
            "^7:vote:suggested:runningmanctf:1": {
                'map': 'runningmanctf',
                'playerid': 1,
            },
            "^7:vote:suggested:runningmanctf": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'vote_suggested')

    def test_get_vote_suggestion_accepted_results(self):
        asserts = {
            "^7:vote:suggestion_accepted:runningmanctf": {
                'map': 'runningmanctf',
            },
            "^7:vote:suggestion_accepte:runningmanctf": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'vote_suggestion_accepted')
