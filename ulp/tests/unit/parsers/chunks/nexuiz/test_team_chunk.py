import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.team as M


class TestTeamChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.TeamChunk()

    def test_get_team_results(self):
        """ test if we're getting proper team results
        """
        asserts = {
            "^7:team:2:14\n": {'playerid': 2, 'team': 14, 'jointype': None},
            "^7:tear:2:14\n": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'team')
