import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.keyhunt as M


class TestKeyhuntChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.KeyhuntChunk()

    def test_get_keyhunt_results(self):
        """ test if we're getting proper results for the keyhunt regex
        """
        asserts = {
            "^7:keyhunt:carrierfrag:1:1:2:0:^1red key": {
                'eventtype': 'carrierfrag', 'playerid': 1, 'playerpoints': 1,
                'keyownerid': 2, 'keyownerpoints': 0, 'teamkey': '^1red key'
            },
            "^7:keyhunt:losekey:2:0:2:0:^1red key": {
                'eventtype': 'losekey', 'playerid': 2, 'playerpoints': 0,
                'keyownerid': 2, 'keyownerpoints': 0, 'teamkey': '^1red key'
            },
            "^7:keyhunt:collect:1:3:0:0:^1red key": {
                'eventtype': 'collect', 'playerid': 1, 'playerpoints': 3,
                'keyownerid': 0, 'keyownerpoints': 0, 'teamkey': '^1red key'
            },
            "^7:keyhunt:capture:1:67:1:0:^3yellow key": {
                'eventtype': 'capture', 'playerid': 1, 'playerpoints': 67,
                'keyownerid': 1, 'keyownerpoints': 0, 'teamkey': '^3yellow key'
            },
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'keyhunt')
