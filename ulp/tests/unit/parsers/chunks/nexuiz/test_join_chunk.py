import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.join as M


class TestJoinChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.JoinChunk()

    def test_get_join_results(self):
        """ test if we're getting proper human join results
        """
        asserts_human = {
            "^7:join:1:1:local:^x112CU^x005|^4dyin^7\n": {
                'human': 1, 'playerid': 1, 'slot': 1, 'ip': '127.0.0.1',
                'name': '^x112CU^x005|^4dyin^7'},
            "^7:join:1:1:1.23.1.234:^x112CU^x005|^4dyin^7\n": {
                'human': 1, 'playerid': 1, 'slot': 1, 'ip': '1.23.1.234',
                'name': '^x112CU^x005|^4dyin^7'},
            "^7:join:1:1:123.456.789.999:^x112CU^x005|^4dyin^7\n": False,
        }

        asserts_ai = {
            "^7:join:3:4:bot:[BOT]Shadow\n": {
                'human': 0, 'playerid': 3, 'slot': 4, 'name': '[BOT]Shadow'
            },
            "^7:join:3:bot:[BOT]Shadow\n": None,
        }

        run_hook_asserts(self, asserts_human, self._tclass.match_line,
                         'invoke_hooks', 'join_human')
        run_hook_asserts(self, asserts_ai, self._tclass.match_line,
                         'invoke_hooks', 'join_ai')
