import unittest

from ulp.tests.utils import run_boolean_asserts, run_equal_asserts
import ulp.parsers.chunks.nexuiz.score as M


class TestScoreChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.ScoreChunk()

    def test_is_scores(self):
        asserts = {
            "^7:scores:dm_runningmanctf:60": True,
            "^7:scores:dm_runningmanctf": False,
        }

        run_boolean_asserts(self, asserts, self._tclass._is_scores)

    def test_get_score_results(self):
        asserts = {
            "^7:scores:dm_runningmanctf:60": {
                'type': 'dm',
                'map': 'runningmanctf',
                'time': 60
            },
            "^7:scores:dm_runningmanctf": None,
        }

        run_equal_asserts(self, asserts, self._tclass._is_scores,
                          self._tclass._get_score_results)

    def test_is_label(self):
        asserts = {
            "^7:labels:player:score!!,kills,deaths<,suicides<,,,,,,": True,
            "^7:labels:player:score!!,caps!,kills,deaths<,suicides<,pickups,\
drops<,fckills,returns,": True,
            "^7:labels:teamscores:caps!!,score,,,,,,,,\n": True,
            "^7:lbls:player:score!!,caps!,kills,deaths<,suicides<,pickups,\
drops<,fckills,returns,": False,
        }

        run_boolean_asserts(self, asserts, self._tclass._is_label)

    def test_get_label_results(self):
        asserts = {
            "^7:labels:player:score!!,kills,deaths<,suicides<,,,,,,": {
                'type': 'player',
                'fields': ['score!!', 'kills', 'deaths<', 'suicides<'],
            },
            "^7:labels:player:score!!,caps!,kills,deaths<,suicides<,pickups,"
            "drops<,fckills,returns,": {
                'type': 'player',
                'fields': ['score!!', 'caps!', 'kills', 'deaths<',
                           'suicides<', 'pickups', 'drops<', 'fckills',
                           'returns']
            },
            "^7:labels:teamscores:caps!!,score,,,,,,,,\n": {
                'type': 'teamscores',
                'fields': ['caps!!', 'score']
            },
            "^7:lbls:player:score!!,caps!,kills,deaths<,suicides<,pickups,\
drops<,fckills,returns,": None,
        }

        run_equal_asserts(self, asserts, self._tclass._is_label,
                          self._tclass._get_label_results)

    def test_is_playerscore(self):
        asserts = {
            "^7:player:see-labels:554,22,20,3,1,25,3,0,0,0:1198:14:10:\
^x112CU^x005|^4dyin^7": True,
            "^7:player:see-labels:0,0,0,0,0,0,0,0,0,0:358:spectator:1:\
^x112CU^x005|^4dyin^7": True,
            "^7:teamscores:see-labels::12": False,
            "^7:teamscores:see-labels:22,548:14": False,
        }

        run_boolean_asserts(self, asserts, self._tclass._is_playerscore)

    def test_get_playerscore_results(self):
        asserts = {
            "^7:player:see-labels:554,22,20,3,1,25,3,0,0,0:1198:14:10:"
            "^x112CU^x005|^4dyin^7": {
                'fields': [554, 22, 20, 3, 1, 25, 3, 0, 0, 0],
                'time': 1198,
                'team': 14,
                'playerid': 10,
                'name': '^x112CU^x005|^4dyin^7',
            },
            "^7:player:see-labels:0,0,0,0,0,0,0,0,0,0:358:spectator:1:"
            "^x112CU^x005|^4dyin^7": {
                'fields': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                'time': 358,
                'team': 'spectator',
                'playerid': 1,
                'name': '^x112CU^x005|^4dyin^7',
            },
            "^7:teamscores:see-labels::12": None,
            "^7:teamscores:see-labels:22,548:14": None,
        }

        run_equal_asserts(self, asserts, self._tclass._is_playerscore,
                          self._tclass._get_playerscore_results)

    def test_is_teamscore(self):
        asserts = {
            "^7:player:see-labels:554,22,20,3,1,25,3,0,0,0:1198:14:10:\
^x112CU^x005|^4dyin^7": False,
            "^7:teamscores:see-labels::12": True,
            "^7:teamscores:see-labels:22,548:14": True,
        }

        run_boolean_asserts(self, asserts, self._tclass._is_teamscore)

    def test_get_teamscore_results(self):
        asserts = {
            "^7:player:see-labels:554,22,20,3,1,25,3,0,0,0:1198:14:10:\
^x112CU^x005|^4dyin^7": None,
            "^7:teamscores:see-labels::12": {
                'fields': None,
                'teamid': 12,
            },
            "^7:teamscores:see-labels:22,548:14": {
                'fields': [22, 548],
                'teamid': 14,
            },
        }

        run_equal_asserts(self, asserts, self._tclass._is_teamscore,
                          self._tclass._get_teamscore_results)
