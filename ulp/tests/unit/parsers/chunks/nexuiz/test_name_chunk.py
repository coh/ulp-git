import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.name as M


class TestNameChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.NameChunk()

    def test_get_name_results(self):
        """ test if we're getting proper results for the name change regex
        """
        asserts = {
            "^7:name:1:Foobar\n": {'playerid': 1, 'newname': 'Foobar'},
            "^7:name:2:\n": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'name')
