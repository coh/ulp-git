import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.end as M


class TestEndChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.EndChunk()

    def test_end_chunk(self):
        asserts = {
            "^7:end": [],
            "^7:err": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'end')
