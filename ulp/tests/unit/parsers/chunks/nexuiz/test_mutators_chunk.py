import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.mutators as M


class TestMutatorsChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.MutatorsChunk()

    def test_get_gameinfo_mutator_results(self):
        """ test if the gameinfo mutator regex is working
        """
        asserts = {
            "^7:gameinfo:mutators:LIST:nixnex:vampire": {
                'mutators': ['nixnex', 'vampire'],
            },
            "^7:gameinfo:mutators:LIST:vampire": {
                'mutators': ['vampire'],
            },
            "^7:gameinfo:mutators:LIST": {
                'mutators': None,
            },
            "^7:gameinfo:mutators:": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'mutators')
