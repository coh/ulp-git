import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.recordset as M


class TestRecordsetChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.RecordsetChunk()

    def test_get_recordset_results(self):
        asserts = {
            "^7:recordset:1:35.899994": {'playerid': 1, 'time': 35.9},
            "^7:recordset:1:": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'recordset')
