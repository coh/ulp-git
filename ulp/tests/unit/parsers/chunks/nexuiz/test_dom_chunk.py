import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.dom as M


class TestDomChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.DomChunk()

    def test_dom_chunk(self):
        """ test if we're getting proper results for the the dom regex
        """
        asserts = {
            "^7:dom:taken:0:2": {'previous_team': 0, 'playerid': 2},
            "^7:dom:taken:14:2": {'previous_team': 14, 'playerid': 2},
            "^7:dom:taker:14:2": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'dom_taken')
