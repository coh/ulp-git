import unittest

from ulp.tests.utils import run_boolean_asserts, run_equal_asserts
import ulp.parsers.chunks.nexuiz.vote as M


class TestVoteChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.VoteChunk()

    def test_is_vcall(self):
        asserts = {
            ":vote:vcall:6:^1gotomap greatwall_reloaded3": True,
            ":vote:vcall:16:^1restart": True,
            ":vote:vcal:foo:bar": False
        }

        run_boolean_asserts(self, asserts, self._tclass._is_vcall)

    def test_get_vcall_results(self):
        asserts = {
            ":vote:vcall:6:^1gotomap greatwall_reloaded3": {
                'playerid': 6,
                'vote': '^1gotomap greatwall_reloaded3',
            },
            ":vote:vcall:16:^1restart": {
                'playerid': 16,
                'vote': '^1restart',
            },
            ":vote:vcal:foo:bar": None
        }

        run_equal_asserts(self, asserts, self._tclass._is_vcall,
                          self._tclass._get_vcall_results)

    def test_is_vote_yn_result(self):
        asserts = {
            ":vote:vyes:1:0:0:0:-1": True,
            ":vote:vstop:0": False,
            ":vote:vno:1:2:0:5:3": True,
        }

        run_boolean_asserts(self, asserts, self._tclass._is_vote_result)

    def test_get_vote_yn_result_results(self):
        asserts = {
            ":vote:vyes:1:0:0:0:-1": {
                'result': 'yes',
                'yes': 1,
                'no': 0,
                'abstain': 0,
                'no vote': 0,
                'minimum': -1
            },
            ":vote:vstop:0": None,
            ":vote:vno:1:2:0:5:3": {
                'result': 'no',
                'yes': 1,
                'no': 2,
                'abstain': 0,
                'no vote': 5,
                'minimum': 3
            },
        }

        run_equal_asserts(self, asserts, self._tclass._is_vote_result,
                          self._tclass._get_vote_result_results)

    def test_is_vote_stop(self):
        asserts = {
            ":vote:vyes:1:0:0:0:-1": False,
            ":vote:vstop:0": True,
            ":vote:vno:1:2:0:5:3": False,
        }

        run_boolean_asserts(self, asserts, self._tclass._is_vote_stop)

    def test_get_vote_stop_results(self):
        asserts = {
            ":vote:vyes:1:0:0:0:-1": None,
            ":vote:vstop:0": {'playerid': 0},
            ":vote:vno:1:2:0:5:3": None,
        }

        run_equal_asserts(self, asserts, self._tclass._is_vote_stop,
                          self._tclass._get_vote_stop_results)

    def test_is_vote_login(self):
        asserts = {
            ":vote:vlogin:0": True,
            ":vote:vlog:foo": False,
        }

        run_boolean_asserts(self, asserts, self._tclass._is_vote_login)

    def test_get_vote_login_results(self):
        asserts = {
            ":vote:vlogin:0": {'playerid': 0},
            ":vote:vlog:foo": None,
        }

        run_equal_asserts(self, asserts, self._tclass._is_vote_login,
                          self._tclass._get_vote_login_results)

    def test_is_vote_do(self):
        asserts = {
            ":vote:vdo:1:^1foobar": True,
            ":vote:vd:0:nope": False,
        }

        run_boolean_asserts(self, asserts, self._tclass._is_vote_do)

    def test_get_vote_do_results(self):
        asserts = {
            ":vote:vdo:1:^1foobar": {'playerid': 1, 'do': '^1foobar'},
            ":vote:vd:0:nope": None,
        }

        run_equal_asserts(self, asserts, self._tclass._is_vote_do,
                          self._tclass._get_vote_do_results)
