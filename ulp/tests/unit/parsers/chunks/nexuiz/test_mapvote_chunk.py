import unittest

from ulp.tests.utils import run_boolean_asserts, run_equal_asserts
import ulp.parsers.chunks.nexuiz.mapvote as M


class TestMapvoteChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.MapvoteChunk()

    def test_map_entries(self):
        """ test if mapping of [m,v,m,v] to {m:v, m:v} works
        """
        asserts = {
            ('a', '1', 'b', '2', 'c', '3'): {'a': 1, 'b': 2, 'c': 3},
            ('a', '1', 'b'): {'a': 1},
        }

        run_equal_asserts(self, asserts, M._map_list_to_dict)

    def test_helper_mapvote_split_match_chunk(self):
        """ test whether the mapvote split helper works
        """
        asserts = {
            'foo:::bar': [['foo'], ['bar']],
            'a:1:b:2:c:3:d:4': [['a', '1', 'b', '2', 'c', '3', 'd', '4']],
            '': [],
            'foo:::b:a:r': [['foo'], ['b', 'a', 'r']]
        }

        run_equal_asserts(self, asserts, M._helper_mapvote_split_match_chunk)

    def test_is_mapvote_keeptwo(self):
        """ test if mapvote keeptwo regex works
        """
        asserts = {
            "^7:vote:keeptwo:stormkeep2:0:dieselpower:0:::bluesky:0:\
runningmanctf:0:cpm24_nex_r1:0:strength:0:didn't vote:1\n": True,
            "^7:vote:keeptwo:accident:1:bloodprisonctf:0:::\
triangulate_nex:0:q3mdctf1_nex:0:didn't vote:1": True,
            "^7:vote:finished:stormkeep2:0:dieselpower:0:::bluesky:0:\
runningmanctf:0:cpm24_nex_r1:0:strength:0:didn't vote:1\n": False
        }

        run_boolean_asserts(self, asserts, self._tclass._is_mapvote_keeptwo)

    def test_is_mapvote_finished(self):
        """ test if mapvote finished regex works
        """
        asserts = {
            "^7:vote:keeptwo:stormkeep2:0:dieselpower:0:::bluesky:0:\
runningmanctf:0:cpm24_nex_r1:0:strength:0:didn't vote:1\n": False,
            "^7:vote:finished:stormkeep2:0:::dieselpower:0:didn't vote:1\n":
            True,
            "^7:vote:finished:stormkeep2:10:::dieselpower:0:bluesky:0:\
runningmanctf:0:cpm24_nex_r1:0:strength:0:didn't vote:1\n": True,
        }

        run_boolean_asserts(self, asserts, self._tclass._is_mapvote_finished)

    def test_get_mapvote_keeptwo_results(self):
        """ test if we're getting proper results for keeptwo mapvote events
        """
        testline = "^7:vote:keeptwo:stormkeep2:0:dieselpower:0:::bluesky:0:\
runningmanctf:0:cpm24_nex_r1:0:strength:0:didn't vote:1\n"
        expectres = [
            ('stormkeep2', 0), ('dieselpower', 0),
            ('cpm24_nex_r1', 0), ('strength', 0),
            ('bluesky', 0), ('runningmanctf', 0),
            ('didn\'t vote', 1)
        ]
        tmp = self._tclass._is_mapvote_keeptwo(testline)
        res = self._tclass._get_mapvote_keeptwo_results(tmp)
        self.assertEqual(res, expectres)

    def test_get_mapvote_finished_results(self):
        """ test if we're getting results from a finished mapvote
        """
        testline = "^7:vote:finished:stormkeep2:0:::dieselpower:0:\
didn't vote:1\n"
        expectres = [('stormkeep2', 0), ('dieselpower', 0),
                     ('didn\'t vote', 1)]
        tmp = self._tclass._is_mapvote_finished(testline)
        res = self._tclass._get_mapvote_finished_results(tmp)
        self.assertEqual(res, expectres)
        res = self._tclass._get_mapvote_finished_results(None)
        self.assertEqual(res, [])
