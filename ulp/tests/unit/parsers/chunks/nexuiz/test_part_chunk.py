import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.part as M


class TestPartChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.PartChunk()

    def test_get_part_results(self):
        """ test if we're getting proper part results
        """
        asserts = {
            "^7:part:3\n": {'playerid': 3},
            "^7:port:3\n": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'part')
