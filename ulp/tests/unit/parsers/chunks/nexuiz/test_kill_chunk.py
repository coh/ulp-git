import unittest

from ulp.tests.utils import run_equal_asserts, run_hook_asserts
import ulp.parsers.chunks.nexuiz.kill as M


class TestKillChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.KillChunk()

    def test_get_kill_frag_or_tk_results(self):
        """ test if we're getting proper frag-or-tk results
        """
        asserts = {
            "^7:kill:frag:4:3:type=10003:items=1:victimitems=3\n": {
                'type': 'frag',
                'attackerid': 4,
                'victimid': 3,
                'cod': 10003,
                'attackeritems': {'weapons': 1, 'flags': None, 'runes': None},
                'victimitems': {'weapons': 3, 'flags': None, 'runes': None},
            },
            #['frag', '4', '3', '10003', '1', '3'],
            "^7:killfrag:4:3:type=10003:items=1:victimitems=3\n": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'kill_ft')

    def test_get_playeritems(self):
        """ test if we're getting proper player items
        """
        asserts = {
            '7': {'weapons': 7, 'flags': None, 'runes': None},
            '1F': {'weapons': 1, 'flags': 'F', 'runes': None},
            '9F|1': {'weapons': 9, 'flags': 'F', 'runes': 1}
        }

        run_equal_asserts(self, asserts,
                          self._tclass._playeritems_dict)

    def test_get_kill_accident_or_suicide_results(self):
        """ test if the frag-or-tk regex works
        """
        asserts = {
            "^7:kill:suicide:1:1:type=10014:items=7\n": {
                'type': 'suicide',
                'attackerid': 1,
                'victimid': 1,
                'cod': 10014,
                'victimitems': {'weapons': 7, 'flags': None, 'runes': None},
            },
            "^7:killfrag:4:3:type=10003:items=1:victimitems=3\n": None,
            "^7:kill:accident:3:3:type=10003:items=8\n": {
                'type': 'accident',
                'attackerid': 3,
                'victimid': 3,
                'cod': 10003,
                'victimitems': {'weapons': 8, 'flags': None, 'runes': None},
            }
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'kill_as')
