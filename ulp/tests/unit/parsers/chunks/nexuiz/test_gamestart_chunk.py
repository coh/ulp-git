import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.gamestart as M


class TestGamestartChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.GamestartChunk()

    def test_get_gamestart_results(self):
        """ test if we're getting proper gamestart results
        """
        asserts = {
            "^7:gamestart:ctf_Desert_Cemetary_q3wc2:0.0.778585": {
                'type': 'ctf',
                'map': 'Desert_Cemetary_q3wc2',
                'id': '0.0.778585'
            },
            "^7:gamestart:dm_basement:0.0.902568": {
                'type': 'dm',
                'map': 'basement',
                'id': '0.0.902568'
            },
            "^7:gamestrt:dm_basement:0.0.902568": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'gamestart')
