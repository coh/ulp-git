import unittest

from ulp.tests.utils import run_hook_asserts
import ulp.parsers.chunks.nexuiz.logversion as M


class TestLogversionChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.LogversionChunk()

    def test_logversion_chunk(self):
        asserts = {
            ":logversion:3": 3,
            ":logvers:1": None,
        }

        run_hook_asserts(self, asserts, self._tclass.match_line,
                         'invoke_hooks', 'logversion')
