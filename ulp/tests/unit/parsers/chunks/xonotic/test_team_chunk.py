import unittest

from ulp.tests.utils import run_boolean_asserts, run_equal_asserts
import ulp.parsers.chunks.xonotic.team as M


class TestTeamChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.TeamChunk()

    def test_is_team(self):
        """ test if the team regex is working
        """
        asserts = {
            "^7:team:2:14:1\n": True,
            "^7:team:2:14\n": False,
            "^7:tear:2:14:1\n": False,
        }

        run_boolean_asserts(self, asserts, self._tclass._is_team)

    def test_get_team_results(self):
        """ test if we're getting proper team results
        """
        asserts = {
            "^7:team:2:14:1\n": {'playerid': 2, 'team': 14, 'jointype': 1},
            "^7:team:2:14\n": None,
            "^7:tear:2:14:1\n": None,
        }

        run_equal_asserts(self, asserts, self._tclass._is_team,
                          self._tclass._get_team_results)
