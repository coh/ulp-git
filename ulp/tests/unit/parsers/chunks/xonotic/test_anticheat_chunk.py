import unittest

from ulp.tests.utils import run_boolean_asserts, run_equal_asserts
import ulp.parsers.chunks.xonotic.anticheat as M


class TestAnticheatChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = M.AnticheatChunk()

    def test_is_anticheat(self):
        """ test if the team regex is working
        """
        asserts = {
            "^7:anticheat:speedhack:7:0": True,
            "^7:anticheat:div0_strafebot_old:7:0.300486": True,
            "^7:anticheat:div0_strafebot_new:7:0.497329": True,
            "^7:anticheat:div0_evade:7:0": True,
            # next one's True due to the way it is parsed
            "^7:anticheat:doesnt_exist:7:0": True,
            "^7:anticheat:div0_strafebot_old:70.300486": False,
            "^7:anticheat:div0_strafebot_new:70.497329": False,
            "^7:anticheat:div0_evade:70": False,
        }

        run_boolean_asserts(self, asserts, self._tclass._is_anticheat)

    def test_get_anticheat_results(self):
        """ test if we're getting proper team results
        """
        asserts = {
            "^7:anticheat:speedhack:7:0": {
                'type': 'speedhack',
                'playerid': 7,
                'score': 0, },
            "^7:anticheat:div0_strafebot_old:7:0.300486": {
                'type': 'div0_strafebot_old',
                'playerid': 7,
                'score': 0.300486, },
            "^7:anticheat:div0_strafebot_new:7:0.497329": {
                'type': 'div0_strafebot_new',
                'playerid': 7,
                'score': 0.497329, },
            "^7:anticheat:div0_evade:7:0": {
                'type': 'div0_evade',
                'playerid': 7,
                'score': 0.0, },
            # next one due to the way it is parsed
            "^7:anticheat:doesnt_exist:7:0": {
                'type': 'doesnt_exist',
                'playerid': 7,
                'score': 0.0, },
            "^7:anticheat:div0_strafebot_old:70.300486": None,
            "^7:anticheat:div0_strafebot_new:70.497329": None,
            "^7:anticheat:div0_evade:70": None,
        }

        run_equal_asserts(self, asserts, self._tclass._is_anticheat,
                          self._tclass._get_anticheat_results)
