import unittest
import ulp.parsers.chunks.nexuiz.join as R


class TestRegressionsJoinChunk(unittest.TestCase):
    def setUp(self):
        self._tclass = R.JoinChunk()

    def test_proper_is_join(self):
        """ test if the join regex does testing for result prior to accessing
            it for the ip-valid test
        """
        self.assertEqual(self._tclass._is_join(':join:1:2:0.0.0.555:foo'),
                         False)

    def test_proper_get_results(self):
        tmp = self._tclass._is_join('^7:join:2:2:bot:[BOT]Discovery')
        self.assertEqual(self._tclass._get_join_results(tmp),
                         {'human': False, 'name': '[BOT]Discovery',
                          'playerid': 2, 'slot': 2}
                         )
