from __future__ import print_function
from flexmock import flexmock


def run_boolean_asserts(c, array, func, func2=None):
    for (key, value) in array.items():
        tmp = func(key)
        if func2:
            tmp = func2(key)

        if value is True:
            c.assertTrue(tmp, str(key) + ': got ' + str(tmp) + ', expected '
                         + str(value))
        elif value is False:
            c.assertFalse(tmp, str(key) + ': got ' + str(tmp) + ', expected '
                          + str(value))
        else:
            print('warning: found non-boolean expectation in \
run_boolean_asserts, f(%s) == %s' % (key, value))


def run_equal_asserts(c, array, func, func2=None):
    for (key, value) in array.items():
        tmp = func(key)
        if func2:
            tmp = func2(tmp)
        c.assertEqual(tmp, value, str(key) + ': got ' + str(tmp) +
                      ', expected ' + str(value))


def run_hook_asserts(c, asserts, calling, mock_func, hook_name):
    '''run mock tests for parser chunks against an assert dict
    '''
    for key, value in asserts.items():
        mock = flexmock()
        res = None
        if value != None and value != False:
            # we have to explicitly check for None, since some valid
            # chunk results can be []
            mock.should_receive(mock_func).with_args(hook_name, value).once
        else:
            mock.should_receive(mock_func).never
        try:
            res = calling(mock, key)
        except Exception:
            raise
        if value == None or value == False:
            c.assertEqual(res, value)
        else:
            c.assertNotEqual(res, None)
