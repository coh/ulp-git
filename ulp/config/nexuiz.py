from __future__ import print_function
from ulp.parsers.chunked import ChunkedParser
from ulp.sinks.nexuiz.sql_logger import SqlLogger
from ulp.sources.nexuiz.udp import UdpSource

_p = ChunkedParser('ulp/parsers/chunks/nexuiz')

#sinks = [SqlLogger('sqlite:///eventlog.sqlite')]
sinks = [SqlLogger('sqlite://')]

for sink in sinks:
    sink.register_hooks(_p)

parsers = [_p.parse]
#source = UdpSource()
