from setuptools import find_packages, setup
import os
import os.path
from ulp.__project__ import version


# since hg can't track empty directories...
try:
    os.makedirs(os.path.join('ulp', 'doc', 'build'))
except OSError:
    pass


install_reqs = [
    'blockdiag>=0.6.1',
    'flexmock>=0.7.1',
    'nose>=0.11.3',
    'Sphinx>=0.6.8',
    'sphinxcontrib-blockdiag>=0.6.1',
    'SQLAlchemy>=0.6',
]

setup(
    name='ulp',
    version=version(),
    description='Unified (Game-)Log Parser',
    author='Clemens-O. Hoppe',
    author_email='coh@co-hoppe.net',
    url='http://bitbucket.org/coh/ulp/',
    install_requires=install_reqs,
    packages=find_packages('.'),
    test_suite="nose.collector",
)
